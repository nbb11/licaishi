<?php
/**
 * 会员模型
 * User: Administrator
 * Date: 2015/9/23
 * Time: 20:05
 */

namespace Api\Model;

use Common\Model\CommonModel;
use Org\Util\String;
use Think\Model;

class MemberModel extends CommonModel
{
    public $model = 'Member';

    public function reg($data)
    {
        $rules  = array(
            array('verify', 'require', '请输入验证码'), //默认情况下用正则进行验证
            array('phone', 'require', '请输入手机号码', 1),
            array('phone', '/^0?(13|15|17|18|14)[0-9]{9}$/', '请输入正确的手机号码', 1, 'regex'),
            array('pwd', 'require', '请输入密码'),
            array('type', array(0, 1), '用户类型不正确', 1, 'in'),
            array('uuid', 'require', '唯一标识码丢失'),
            array('phone', '', '帐号名称已经存在！', 1, 'unique', 3),
        );
        $member = M('Member');
        if (!$member->validate($rules)->create($data)) {
            return ['code' => __LINE__, 'msg' => $member->getError()];
        }
        $key = md5($data['uuid'] . '_' . $data['phone']);
        if (S($key) != $data['verify']) {
            return ['code' => __LINE__, 'msg' => '验证码不正确或验证码失效'];
        }
        $ref = $member->field('phone,pwd,rtime')->add([
            'phone' => $data['phone'],
            'pwd'   => mkpwd($data['pwd']),
            'rtime' => time()
        ]);
        if ($ref) {
            return ['code' => 0, 'msg' => 'ok'];
        } else {
            return ['code' => __LINE__, 'msg' => '注册异常'];
        }

    }

    public function phone($phone,$type=0)
    {
        if(empty($type)) {
            $rules = array(
                array('phone', 'require', '请输入手机号码', 1),
                array('phone', '/^0?(13|15|17|18|14)[0-9]{9}$/', '请输入正确的手机号码', 1, 'regex'),
                array('phone', '', '帐号名称已经存在！', 1, 'unique', 3),
            );
        }else{
            $rules = array(
                array('phone', 'require', '请输入手机号码', 1),
                array('phone', '/^0?(13|15|17|18|14)[0-9]{9}$/', '请输入正确的手机号码', 1, 'regex'),
            );
        }
        $member = M('Member');
        if (!$member->validate($rules)->create(['phone' => $phone])) {
            return ['code' => __LINE__, 'msg' => $member->getError()];
        } else {
            return ['code' => 0, 'msg' => ''];
        }
    }

    public function login($data)
    {
        $rules  = array(
            array('phone', 'require', '请输入手机号码', 1),
            array('phone', '/^0?(13|15|17|18|14)[0-9]{9}$/', '请输入正确的手机号码', 1, 'regex'),
            array('pwd', 'require', '请输入密码'),
            array('uuid', 'require', '唯一标识码丢失'),
            array('device', array(0, 1), '设备类型不正确', 1, 'in'),
        );
        $member = M('Member');
        if (!$member->validate($rules)->create($data)) {
            return ['code' => __LINE__, 'msg' => $member->getError()];
        }
        $dbinfo = $member->where(['phone' => $data['phone']])->field('muid,pwd,type')->find();
        if (empty($dbinfo)) {
            return ['code' => __LINE__, 'msg' => '手机尚未注册'];
        }
        if (password_verify($data['pwd'], $dbinfo['pwd'])) {
            $token = md5(String::uuid() . $data['uuid']);
            // 更新时间戳
            $ref = $member->where(['muid' => $dbinfo['muid']])->field('device,deviceid,token,ltime')->save([
                'device'   => $data['device'],
                'ltime'    => time(),
                'deviceid' => $data['uuid'],
                'token'    => $token
            ]);
            if ($ref) {
                return ['code' => 0, 'msg' => $token,'type'=>$dbinfo['type'],'uid'=>$dbinfo['muid']];
            } else {
                return ['code' => __LINE__, 'msg' => '登陆异常'];
            }
        } else {
            return ['code' => __LINE__, 'msg' => '登陆密码不正确'];
        }
    }

    /**
     * 密码找回
     */
    public function getpwd($data)
    {
        $rules  = array(
            array('verify', 'require', '请输入验证码'), //默认情况下用正则进行验证
            array('phone', 'require', '请输入手机号码', 1),
            array('phone', '/^0?(13|15|17|18|14)[0-9]{9}$/', '请输入正确的手机号码', 1, 'regex'),
            array('pwd', 'require', '请输入密码'),
            array('uuid', 'require', '唯一标识码丢失'),
        );
        $member = M('Member');
        if (!$member->validate($rules)->create($data)) {
            return ['code' => __LINE__, 'msg' => $member->getError()];
        }
        $key = md5($data['uuid'] . '_' . $data['phone']);
        if (S($key) != $data['verify']) {
            return ['code' => __LINE__, 'msg' => '验证码不正确或验证码失效'];
        }
        $mid = $member->getFieldByPhone($data['phone'], 'muid');
        if (empty($mid)) {
            return ['code' => __LINE__, 'msg' => '手机号码尚未注册'];
        }

        $ref = $member->where(['muid' => $mid])->field('pwd')->save(['pwd' => mkpwd($data['pwd'])]);

        if ($ref) {
            return ['code' => 0, 'msg' => 'ok'];
        } else {
            return ['code' => __LINE__, 'msg' => '密码找回异常'];
        }
    }

    /**
     * 重置密码
     */
    public function resetpwd($data)
    {
        $rules  = array(
            array('token', 'require', '登陆令牌丢失'),
            array('pwd', 'require', '请输入密码'),
            array('oldpwd', 'require', '请输入原始密码'),
        );
        $member = M('Member');
        if (!$member->validate($rules)->create($data)) {
            return ['code' => __LINE__, 'msg' => $member->getError()];
        }
        $dbpwd = $member->getFieldByToken($data['token'], 'pwd');
        if (empty($dbpwd)) {
            return ['code' => __LINE__, 'msg' => '登陆令牌失效'];
        }
        if (password_verify($data['oldpwd'], $dbpwd)) {
            $ref = $member->where(['token' => $data['token']])->field('pwd')->save(['pwd' => mkpwd($data['pwd'])]);
            if ($ref) {
                return ['code' => 0, 'msg' => 'ok'];
            } else {
                return ['code' => __LINE__, 'msg' => '密码修改异常'];
            }
        } else {
            return ['code' => __LINE__, 'msg' => '原始密码不正确'];
        }
    }
}
