<?php
/**
 * 用户注册
 * User: Administrator
 * Date: 2015/9/24
 * Time: 0:32
 */

namespace Api\Controller;

use Org\Util\String;
use Think\Controller\RestController;
use Think\Log;

class RegisterController extends RestController
{
    /**
     * 手机校验合法性
     */
    public function checkphone()
    {
        $phone  = I('post.phone');
        $member = D('Member');
        $this->response($member->phone($phone), 'json');
    }

    /**
     * 短信验证码发送
     * todo: 验证码有效期暂且设置为5分钟
     * 前端验证码失效时间建议为3分钟
     */
    public function msg()
    {
        $phone = I('post.phone');
        // 设备唯一码
        $uuid   = I('post.uuid');
        $member = D('Member');
        $ref    = $member->phone($phone,1);
        if (!empty($ref['code'])) {
            $this->response($ref, 'json');
        }
        if (empty($uuid)) {
            $this->response(['code' => __LINE__, 'msg' => '设备标识符丢失'], 'json');
        }
        // 发送验证码
        $key  = md5($uuid . '_' . $phone);
        $code = S($key);
        if(empty($code)) {
            $code = String::randString(4, 1);
            S($key, $code, 300);
        }
        $msg = '本次验证码[' . $code . '],五分钟有效';
        $ref=$member->sendSMS($phone, $msg);
        //Log::INFO($phone.'发送验证码['.$code.']短信系统返回['.$ref.']');
        // 返回数据
        $this->response(
            ['code' => 0, 'msg' => '', 'data' => ['deadline' => 600]], 'json');

    }

    /**
     * 密码找回
     */
    public function getpwd()
    {
        $data   = [
            'phone'  => I('post.phone'),
            'pwd'    => I('post.pwd'),
            'verify'   => I('post.verify'),
            'uuid'   => I('post.uuid')
        ];
        $member        = D('Member');
        $ref           = $member->getpwd($data);
        $this->response($ref, 'json');
    }

    /**
     * 重置密码
     */
    public function resetpwd()
    {
        $data   = [
            'token'  => I('post.token'),
            'pwd'    => I('post.pwd'),
            'oldpwd'   => I('post.oldpwd')
        ];
        $member        = D('Member');
        $ref           = $member->resetpwd($data);
        $this->response($ref, 'json');
    }

    /**
     * 用户登录
     */
    public function login()
    {
        $data   = [
            'phone'  => I('post.phone'),
            'pwd'    => I('post.pwd'),
            'device'   => I('post.device'),
            'uuid'   => I('post.uuid'),
            'deviceid'   => I('post.deviceid'),
        ];
        $member        = D('Member');
        $ref           = $member->login($data);
        if (!empty($ref['code'])) {
            $this->response($ref, 'json');
        } else {
            $this->response(['code'=>0,'msg'=>'ok','data'=>['token'=>$ref['msg'],'type'=>$ref['type'],'GUID'=>$ref['uid']]],'json');
        }
    }

    /**
     * 用户注册
     */
    public function doreg()
    {
        $member = D('Member');
        $data   = [
            'verify' => I('post.verify'),
            'phone'  => I('post.phone'),
            'pwd'    => I('post.pwd'),
            'type'   => I('post.type'),
            'uuid'   => I('post.uuid')
        ];
        $ref    = $member->reg($data);
        $this->response($ref, 'json');
    }
}
