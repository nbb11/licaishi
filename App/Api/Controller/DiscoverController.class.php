<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/10/12
 * Time: 17:07
 */

namespace Api\Controller;


use Think\Controller\RestController;

class DiscoverController extends RestController
{
    /**
     * 获取观点
     */
    public function viewpoint()
    {
        $db        = M('Point');
        $total     = $db->where(['status' => 1])->count();
        $pageCount = I('pageCount', 1, 'intval');
        $pageCount = max(1, $pageCount);
        $showCount = I('showCount', 15, 'intval');
        $data      = $db->field([
            'avatar as iconUrl',
            'nickname as name',
            'reply as replyCount',
            'msg as text',
            'atime as time',
            'title',
            'id as url'
        ])->join('__MEMBER__ ON __POINT__.muid=__MEMBER__.muid',
            'LEFT')->where(['status' => 1])->limit(($pageCount - 1) * $showCount, $showCount)->select();
        if (empty($data)) {
            $this->response(['code' => __LINE__, 'msg' => '暂无数据'], 'json');
        }
        foreach ($data as $k => $v) {
            $v['id']=$v['url'];
            $v['time']    = date('Y-m-d H:i:s', $v['time']);
            $v['url']     = U('Home/News/vpoint', ['id' => $v['url']], 'html', true);
            $v['iconUrl'] = U($v['iconUrl'], '', '', true);
            $data[$k]     = $v;

        }
        $this->response(['code' => 0, 'data' => ['list' => $data, 'totalCount' => $total]], 'json');
    }

    /**
     * 置顶信息
     */
    public function activity()
    {
        $dbactivity = M('Activity');
        $total      = $dbactivity->where(['status' => 0])->count();
        $pageCount  = I('pageCount', 1, 'intval');
        $pageCount  = max(1, $pageCount);
        $showCount  = I('showCount', 15, 'intval');
        if (empty($total)) {
            $this->response(['code' => __LINE__, 'msg' => '暂无数据'], 'json');
        } else {
            $data = $dbactivity->field([
                'title',
                'img as imageUrl',
                'endtime as endTime',
                'cid'
            ])->where(['status' => 0,'endtime'=>['EGT',time()]])->limit((($pageCount - 1) * $showCount),
                $showCount)->order('ctime desc')->select();
            if (empty($data)) {
                $this->response(['code' => __LINE__, 'msg' => '暂无数据'], 'json');
            }
            foreach ($data as $k => $val) {
                $v        = [
                    'endTime'  => empty($val['endTime']) ? date('Y-m-d H:i:s', (time() + 89000)) : date('Y-m-d H:i:s',
                        $val['endTime']),
                    'imageUrl' => empty($val['imageUrl']) ? '' : U($val['imageUrl'], '', '', true),
                    'url'      => U('Home/News/activity', ['id' => $val['cid']], 'html', true),
                    'title'    => $val['title']
                ];
                $data[$k] = $v;
            }
        }
        $this->response(['code' => 0, 'data' => ['list' => $data, 'totalCount' => $total]], 'json');
    }

    /**
     * 置顶信息
     */
    public function hoitpoint()
    {
        $db   = M('Point');
        $data = $db->field([
            'title',
            'id'
        ])->where(['status' => 1, 'ishot' => 1])->order('atime desc')->select();
        if (empty($data)) {
            $this->response(['code' => __LINE__, 'msg' => '暂无数据'], 'json');
        }
        foreach ($data as $k => $val) {
            $v        = [
                'id'       => $val['id'],
                'title'    => $val['title'],
                'URL'      => U('Home/News/vpoint', ['id' => $val['id']], 'html', true),
                'imageUrl' => 'http://p0.so.qhimg.com/bdr/_240_/t014504431eb9867a3a.jpg'
            ];
            $data[$k] = $v;
        }
        $this->response(['code' => 0, 'data' => ['list' => $data]], 'json');
    }
}