<?php

/**
 * 手机客户端API
 * User: Administrator
 * Date: 2015/9/21
 * Time: 23:40
 */
namespace Api\Controller;

use Think\Controller\RestController;

class IndexController extends RestController
{
    /**
     * 默认加载相关配置
     */
    public function index()
    {
        // 载入预测
        $dbmod   = M('MarketForecastLog');
        $time    = date('Y-m-d');
        $data    = $dbmod->field('type,count(id) as total')->where(['time' => $time])->group('type')->select();
        $reponse = ['rise' => 0, 'fall' => 0, 'flat' => 0];
        // banner
        if (!empty($data)) {
            $type = [1 => 'rise', 2 => 'flat', 3 => 'fall'];
            foreach ($data as $k => $v) {
                $reponse[$type[$v['type']]] = $v['total'];
            }
        }
        $dbbanner = M('Banner');
        $banner   = $dbbanner->field([
            'bid as bannerid',
            'href as link_url',
            'imgs as url'
        ])->where(['type' => 0])->select();
        foreach ($banner as $k => $v) {
            $v['url']   = U($v['url'], '', '', true);
            $banner[$k] = $v;
        }
        $reponse['bannerList'] = empty($banner) ? [] : $banner;
        $this->response(['code' => 0, 'msg' => 'ok', 'data' => $reponse], 'json');

    }

    /**
     * 股票搜索
     */
    public function stock()
    {
        $keyword     = I('time');
        $dbstock     = M('Stock');
        $data        = $dbstock->max('uptime');
        $map         = [];
        $map['type'] = ['ELT', 31];
        if (!empty($keyword)) {
            $keyword = strtotime($keyword);
            if ($keyword >= $data) {
                $this->response(['code' => __LINE__, 'msg' => '暂无更新'], 'json');
            } else {
                $map['uptime'] = ['GT', $keyword];
            }
        }
        $dbdata = $dbstock->where($map)->select();
        foreach ($dbdata as $val) {
            $list[] = [
                'ssid'   => $val['code'],
                'name'   => $val['name'],
                'type'   => $val['type'],
                'pinyin' => $val['pycode'],
                'flag'   => '0'
            ];
        }
        // 如果是字母就检错
        $data = [
            'code' => 0,
            'msg'  => 'ok',
            'data' => [
                'pageCount' => count($list),
                'list'      => $list,
                'time'      => date('Y-m-d H:i:s', $data)
            ]
        ];
        $this->response($data, 'json');
    }

    public function stype()
    {
        $data = [
            0  => '上证指数',
            1  => '上证A股',
            2  => '上证B股',
            3  => '深圳指数',
            4  => '深圳A股',
            5  => '深圳B股',
            6  => '上证债券',
            7  => '深圳债券',
            8  => '上证基金',
            9  => '深证基金',
            10 => '二板指数',
            12 => '板块指数',
            13 => '沪深AG',
            14 => '沪深BG',
            15 => '中小板',
            16 => '创业板',
            17 => '基金',
            18 => '债券',
            31 => '全球指数',
            32 => '外汇',
            33 => '全球股指期货',
            34 => '全球外汇期货',
            35 => '全球贵金属期货',
            36 => '全球工业品期货',
            37 => '全球农产品期货',
            38 => '全球能源期货',
            39 => '国债期货',
            40 => '国内股指期货',
            41 => '热轧卷板期货',
            42 => '沥青期货',
            43 => '黄金期货',
            44 => '白银期货',
            45 => '沪锌期货',
            46 => '燃油期货',
            47 => '橡胶期货',
            49 => '沪铝期货',
            50 => '沪铜期货',
            51 => '线材期货',
            52 => '螺纹钢期货',
            53 => '沪铅期货',
            54 => '聚丙烯期货',
            55 => '纤维板期货',
            56 => '胶合板期货',
            57 => '鸡蛋期货',
            58 => '铁矿石期货',
            59 => '焦煤期货',
            60 => 'PVC期货',
            61 => '乙烯期货',
            62 => '豆油期货',
            63 => '豆一期货',
            64 => '玉米期货',
            65 => '豆二期货',
            66 => '豆粕期货',
            67 => '棕榈期货',
            68 => '焦炭期货',
            69 => '锰硅期货',
            70 => '硅铁期货',
            71 => '晚籼稻期货',
            72 => '粳稻期货',
            73 => '动力煤期货',
            74 => '玻璃期货',
            75 => '甲醇期货',
            76 => '棉花期货',
            77 => '菜油期货',
            78 => '菜粕期货',
            79 => '菜籽期货',
            80 => '甲酸期货',
            81 => '白糖期货',
            82 => '强麦期货',
            83 => '籼稻期货',
            84 => '渤海商品期货',
            85 => '上海黄金交易所行情',
            86 => '实物黄金行情',
            87 => '外盘黄金行情',
        ];

        $this->response(['code' => 0, 'data' => $data], 'json');
    }
}
