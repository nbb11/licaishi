<?php
/**
 * 用户关注
 * User: phpboy
 * Date: 2015/9/24
 * Time: 21:49
 */

namespace Api\Controller;


class FollowController extends BeforController
{
    /**
     * 新增关注
     */
    public function add(){
        $flag = I('post.flag',0,'intval');
        if(empty($flag)){
            $this->del();
        }else{
        $mid = I('GUID');
        if(empty($mid)){
            $this->response(['code'=>__LINE__,'msg'=>'请输入牛人uid'],'json');
        }
        $member = M('Member');
        $mtype = $member->getFieldByMuid($mid, 'type');
        if(empty($mtype)){
            $this->response(['code'=>__LINE__,'msg'=>'牛人不存在'],'json');
        }
        $ck = md5($mid.'_'.$this->_userinfo['muid']);
        $follow = M('Follow');
        $ischeck = $follow->getFieldById($ck,'id');
        if(!empty($ischeck)){
            $this->response(['code'=>__LINE__,'msg'=>'已经关注该牛人'],'json');
        }
        $ref=$follow->data(['id'=>$ck,'nbuid'=>$mid,'muid'=>$this->_userinfo['muid'],'addtime'=>time()])->add();
        if($ref){
            $this->response(['code'=>0,'msg'=>'ok'],'json');
        }else{
            $this->response(['code'=>__LINE__,'msg'=>'操作异常']);
        }
        }
    }

    /**
     * 取消关注
     */
    public function del(){
        $mid = I('GUID');
        if(empty($mid)){
            $this->response(['code'=>__LINE__,'msg'=>'请输入牛人uid'],'json');
        }
        $member = M('Member');
        $mtype = $member->getFieldByMuid($mid, 'type');
        if(empty($mtype)){
            $this->response(['code'=>__LINE__,'msg'=>'牛人不存在'],'json');
        }
        $ck = md5($mid.'_'.$this->_userinfo['muid']);
        $follow = M('Follow');
        $ischeck = $follow->getFieldById($ck,'id');
        if(empty($ischeck)){
            $this->response(['code'=>__LINE__,'msg'=>'尚未关注该牛人'],'json');
        }
        $ref=$follow->delete($ck);
        if($ref){
            // 统计关注
            $this->response(['code'=>0,'msg'=>'ok'],'json');
        }else{
            $this->response(['code'=>__LINE__,'msg'=>'操作异常'],'json');
        }
    }

    /**
     * 关注列表
     */
    public function lists(){
        $showCount = I('post.showCount', 15, 'intval');
        $pageCount = I('post.pageCount', 1, 'intval');
        $pageCount = max($pageCount, 1);
        $dbfollow  = M('Follow');
        // 查看订阅列表
        $count = $dbfollow->where(['muid' => $this->_userinfo['muid']])->count();
        if (empty($count)) {
            $this->response(['code' => __LINE__, 'msg' => '暂无关注'], 'json');
        }
        $data = $dbfollow->field('nbuid')->where(['muid' => $this->_userinfo['muid']])->limit((($pageCount - 1) * $showCount) . ',' . $showCount)->select();
        if (empty($data)) {
            $this->response(['code' => __LINE__, 'msg' => '暂无关注'], 'json');
        }
        $muid  = [];
        foreach ($data as $k => $v) {
            $muid[] = $v['nbuid'];
        }
        $dbmember = M('Member');
        $data     = $dbmember->field([
            'hpkg_member.muid as muid',
            'follow as attentionCount',
            'company as company',
            'office as office',
            'about as introduction',
            'phone as phone',
            'sex as sex',
            'subscribe as subscriptionCount',
            'nickname as username',
            'avatar as headimgUrl'
        ])->where([
            'hpkg_member.muid' => [
                'in',
                $muid
            ]
        ])->join('__MEMBER_COUNT__ ON __MEMBER_COUNT__.mid=__MEMBER__.muid',
            'LEFT')->join('__PLANNER__ ON __PLANNER__.muid=__MEMBER__.muid', 'LEFT')->select();

        foreach($data as $k=>$v){
            $v['headimgUrl']=empty($v['headimgUrl'])?'':U($v['headimgUrl'], '', '', true);
            $data[$k]=$v;
        }

        $this->response(['code' => 0, 'data' => ['totalPage' => $count, 'list' => $data]], 'json');
    }
}