<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/9/23
 * Time: 19:40
 */

namespace Api\Controller;
use Think\Controller\RestController;

class BeforController extends RestController
{
    protected $_userinfo='';
    /**
     * 判断是否登陆
     */
    public function _initialize(){
        $token = I('token','');
        $member = M('Member');
        $info = $member->where(['muid'=>$token])->find();
        import('Com.Requests');
        \Requests::register_autoloader();
        $this->_userinfo= $info;
    }

    /**
     * 检查股票交易时间
     */
    protected function checktime(){
        if(in_array(date('N'),[6,7])){
            return false;
        }else{
            $now = time();
            $nowdate = date('Y-m-d');
            $boenkey = [strtotime($nowdate.' 09:30'),strtotime($nowdate.' 11:30'),strtotime($nowdate.' 13:00'),strtotime($nowdate.' 15:00')];
            if(($now>=$boenkey[0] && $now<=$boenkey[1])||($now>=$boenkey[2] && $now<=$boenkey[3])){
                return true;
            }else{
                return false;
            }
        }
    }
}
