<?php
/**
 * 客户端接口配置文件
 * auth: shaoqi
 * Date: 2015/9/22
 * Time: 21:32
 * ceshi
 */
return array(
    'URL_ROUTER_ON'   => true,
    'URL_MODEL'       => 2,
    'URL_ROUTE_RULES' => array(
        // 牛人动态
        array('index/trends', 'member/trends', '', array('ext' => 'json', 'method' => 'post')),
        // 首页加载配置
        array('sys/config', 'index/index', '', array('ext' => 'json', 'method' => 'post')),
        array('search/stock', 'index/stock', '', array('ext' => 'json', 'method' => 'post')),
        array('search/member', 'member/search', '', array('ext' => 'json', 'method' => 'post')),
        // 撤销关注
        array('follow/del', 'follow/del', '', array('ext' => 'json', 'method' => 'post')),
        // 关注列表
        array('follow/lists', 'follow/lists', '', array('ext' => 'json', 'method' => 'post')),
        // 关注
        array('member/follow', 'follow/add', '', array('ext' => 'json', 'method' => 'post')),
        // 取消订阅
        array('subscribe/del', 'subscribe/del', '', array('ext' => 'json', 'method' => 'post')),
        // 订阅列表
        array('subscribe/lists', 'subscribe/lists', '', array('ext' => 'json', 'method' => 'post')),
        // 订阅
        array('member/subscribe', 'subscribe/add', '', array('ext' => 'json', 'method' => 'post')),
        // 登录
        array('member/login', 'register/login', '', array('ext' => 'json', 'method' => 'post')),
        // 注册
        array('member/register', 'register/doreg', '', array('ext' => 'json', 'method' => 'post')),
        // 校验手机号是否正确
        array('check/phone', 'register/checkphone', '', array('ext' => 'json', 'method' => 'post')),
        // 验证码发送
        array('check/sendmsg', 'register/msg', '', array('ext' => 'json', 'method' => 'post')),
        // 密码找回
        array('login/getpwd', 'register/getpwd', '', array('ext' => 'json', 'method' => 'post')),
        // 重置密码
        array('login/resetpwd', 'register/resetpwd', '', array('ext' => 'json', 'method' => 'post')),
        // 自选行情
        array('optional/stock', 'freestock/stock', '', array('ext' => 'json', 'method' => 'post')),
        // 自选大盘
        array('optional/exponential', 'freestock/exponential', '', array('ext' => 'json', 'method' => 'post')),
        // 自选资金
        array('optional/capital', 'freestock/capital', '', array('ext' => 'json', 'method' => 'post')),
        // 自选新闻
        array('optional/news', 'freestock/news', '', array('ext' => 'json', 'method' => 'post')),
        // 自选研报
        array('optional/reports', 'freestock/reports', '', array('ext' => 'json', 'method' => 'post')),
        // 自选同步
        array('optional/sync', 'freestock/sync', '', array('ext' => 'json', 'method' => 'post')),
        // 行情走势
        array('market/sotck', 'stock/timeline', '', array('ext' => 'json', 'method' => 'post')),
        // 行情跌幅
        array('market/drop', 'stock/drop', '', array('ext' => 'json', 'method' => 'post')),
        // 行情大盘
        array('market/grail', 'stock/grail', '', array('ext' => 'json', 'method' => 'post')),
        // 大盘信息
        array('market/timeline', 'stock/gtimeline', '', array('ext' => 'json', 'method' => 'post')),
        // 买入股票
        array('stock/buy', 'freestock/buy', '', array('ext' => 'json', 'method' => 'post')),
        // 我关注的股票
        array('stock/follow', 'freestock/optional', '', array('ext' => 'json', 'method' => 'post')),
        // 卖出股票
        array('stock/sell', 'freestock/offtake', '', array('ext' => 'json', 'method' => 'post')),
        // 获取股票类型 枚举
        array('stock/types', 'index/stype', '', array('ext' => 'json', 'method' => 'get')),
        // 发现-观点
        array('discover/viewpoint', 'discover/viewpoint', '', array('ext' => 'json', 'method' => 'post')),
        // 发现-有奖活动
        array('discover/activity', 'discover/activity', '', array('ext' => 'json', 'method' => 'post')),
        // 发现-观点置顶
        array('discover/hoitpoint', 'discover/hoitpoint', '', array('ext' => 'json', 'method' => 'post')),
        // 发布观点
        array('point/push', 'freestock/point', '', array('ext' => 'json', 'method' => 'post')),
        // 发布视频
        array('video/push', 'freestock/video', '', array('ext' => 'json', 'method' => 'post')),
        // 视频点赞
        array('video/recommend', 'freestock/vsub', '', array('ext' => 'json', 'method' => 'post')),
        // 视频列表
        array('video/lists', 'freestock/vlist', '', array('ext' => 'json', 'method' => 'post')),
        // 视频观看记录
        array('video/logs', 'freestock/vlog', '', array('ext' => 'json', 'method' => 'post')),
        // 修改用户头像
        array('member/avatar', 'member/avatar', '', array('ext' => 'json', 'method' => 'post')),
        // 获取用户基本信息
        array('member/info', 'member/info', '', array('ext' => 'json', 'method' => 'post')),
        // 修改用户资料
        array('member/change', 'member/change', '', array('ext' => 'json', 'method' => 'post')),
        // 问答列表
        array('ask/lists', 'member/alists', '', array('ext' => 'json', 'method' => 'post')),
        // 问答回复
        array('ask/reply', 'member/areply', '', array('ext' => 'json', 'method' => 'post')),
        // 金币任务
        array('tasks/lists', 'member/tasks', '', array('ext' => 'json', 'method' => 'post')),
        // 金币任务
        array('invest/history', 'invest/history', '', array('ext' => 'json', 'method' => 'post')),
        // 金币任务
        array('invest/info', 'invest/info', '', array('ext' => 'json', 'method' => 'post')),
        // 金币任务
        array('invest/position', 'invest/position', '', array('ext' => 'json', 'method' => 'post')),
        // 交易记录
        array('invest/logs', 'invest/rawlogs', '', array('ext' => 'json', 'method' => 'post')),
        // 发帖
        array('thread/posts', 'stock/thread', '', array('ext' => 'json', 'method' => 'post')),
    ),
);