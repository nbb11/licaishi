<?php

/**
 * @author 小策一喋 <xvpindex@qq.com>
 * @link http://www.topstack.cn
 * @copyright Copyright (C) 2014 EWSD
 * @datetime 2014-10-15 12:40
 * @version 1.0
 * @description
 */

namespace Home\Controller;

use Think\Controller;

class IndexController extends CommonController {

    /**
      +----------------------------------------------------------
     * 定义
      +----------------------------------------------------------
     */
    protected $model;

    /**
      +----------------------------------------------------------
     * 初始化
      +----------------------------------------------------------
     */
    public function _initialize() {
        parent::_initialize();
        $this->channelModel = D('Admin/Channel');
        $this->articleModel = D('Admin/Article');
    }

    public function index() {
        $Dao = M();
        $list = $Dao -> query("select * from hpkg_member where type = 1 limit 0,9");
        $this->assign("personList", $list);

        $this->display('index');
    }

    public function index3() {

    	$articleMap['title|content'] = array('LIKE', '%' . I('get.kw') . '%');
        $articleList = $this->articleModel->getListbyAll($order = 'uTime DESC', $articleMap);
        $this->ajaxReturn($articleList);

    }
    
    public function register(){
    	$this->display();
    }
    
    public function doReg($username, $password, $verifycode){
        $Dao = M();
        $list = $Dao -> query("select * from hpkg_member where phone = '".$username."'");
        if(count($list) > 0){
            $this -> error("此用户已经注册！");
        }else{
            $sql = "insert into hpkg_member(phone,nickname,pwd,type) values('".$username."','".$username."','".mkpwd($password)."',0)";
            $Dao -> execute($sql);
            $this -> success();
        }
    }
    
    public function login(){
    	$this -> display();
    }

    public function doLogin($username, $password){
        $Dao = M();
        $sql = "select * from hpkg_member where phone='".$username."'";
        $member = $Dao -> query($sql);
        if(count($member) > 0){
            $member = $member[0];
            if(password_verify($password, $member['pwd'])){
                $_SESSION['member'] = $member;
                $this -> success();
            }else{
                $this -> error("密码错误！");
            }
        }else{
            $this -> error("此用户不存在！");
        }
    }

    public function loginout(){
        $_SESSION['member'] = null;
        $this -> redirect("/");
    }
    
    public function ask(){
    	$this -> display();
    }
    
    public function clcp(){
    	$this -> display();
    }
    
    public function media(){
    	$this -> display();
    }
    
    public function main(){
        $member = $_SESSION['member'];
        $this -> assign("member", $member);
    	$this -> display();
    }
    
    public function main_msg(){
    	$this -> display();
    }
    
    public function nav(){
    	$this -> display("_nav");
    }
    
    public function sq(){
    	$this -> display();
    }
    
    public function person($id){
        $Dao = M();
        $person = $Dao -> query("select * from hpkg_member where muid=".$id);
        $this -> assign("person", $person);

        $gdList = $Dao -> query("select * from hpkg_point where muid=".$id);
        $this -> assign("gdList", $gdList);
        $this -> assign("gdCount", count($gdList));

        $wdList = $Dao -> query("select * from hpkg_qa where aid=".$id);
        $this -> assign("wdList", $wdList);
        $this -> assign("wdCount", count($wdList));
    	$this -> display();
    }

}

?>