<?php
/**
 * 命令行运行
 *
 * @author    phpboy <admin@shaoqi.net>
 * @link      http://www.shaoqi.net
 * @copyright Copyright (C) 2015
 * @datetime  2015-09-28 12:40
 * @version   1.0
 * @description
 */

namespace Home\Controller;

use Think\Controller;

class CliController extends Controller
{

    function index()
    {
        echo 'stock db sync running' . date('Y-m-d H:i:s') . PHP_EOL;
        // 同步 token
        import('Common.Util.Stock');
        $stock    = new \Stock();
        $stockkey = [0  => '上证指数',
                     1  => '上证A股',
                     2  => '上证B股',
                     3  => '深圳指数',
                     4  => '深圳A股',
                     5  => '深圳B股',
                     /*6  => '上证债券',
                     7  => '深圳债券',
                     8  => '上证基金',
                     9  => '深证基金',
                     10 => '二板指数',
                     12 => '板块指数',
                     13 => '沪深AG',
                     14 => '沪深BG',*/
                     15 => '中小板',
                     16 => '创业板',
                     /*17 => '基金',
                     18 => '债券',*/
                     //31 => '全球指数',
                     /*32 => '外汇',
                     33 => '全球股指期货',
                     34 => '全球外汇期货',
                     35 => '全球贵金属期货',
                     36 => '全球工业品期货',
                     37 => '全球农产品期货',
                     38 => '全球能源期货',
                     39 => '国债期货',
                     40 => '国内股指期货',
                     41 => '热轧卷板期货',
                     42 => '沥青期货',
                     43 => '黄金期货',
                     44 => '白银期货',
                     45 => '沪锌期货',
                     46 => '燃油期货',
                     47 => '橡胶期货',
                     49 => '沪铝期货',
                     50 => '沪铜期货',
                     51 => '线材期货',
                     52 => '螺纹钢期货',
                     53 => '沪铅期货',
                     54 => '聚丙烯期货',
                     55 => '纤维板期货',
                     56 => '胶合板期货',
                     57 => '鸡蛋期货',
                     58 => '铁矿石期货',
                     59 => '焦煤期货',
                     60 => 'PVC期货',
                     61 => '乙烯期货',
                     62 => '豆油期货',
                     63 => '豆一期货',
                     64 => '玉米期货',
                     65 => '豆二期货',
                     66 => '豆粕期货',
                     67 => '棕榈期货',
                     68 => '焦炭期货',
                     69 => '锰硅期货',
                     70 => '硅铁期货',
                     71 => '晚籼稻期货',
                     72 => '粳稻期货',
                     73 => '动力煤期货',
                     74 => '玻璃期货',
                     75 => '甲醇期货',
                     76 => '棉花期货',
                     77 => '菜油期货',
                     78 => '菜粕期货',
                     79 => '菜籽期货',
                     80 => '甲酸期货',
                     81 => '白糖期货',
                     82 => '强麦期货',
                     83 => '籼稻期货',
                     84 => '渤海商品期货',
                     85 => '上海黄金交易所行情',
                     86 => '实物黄金行情',
                     87 => '外盘黄金行情',*/
        ];
        $dbstock  = M('stock');
        foreach ($stockkey as $k => $v) {
            if ($k > 31) {
                //break;
            }
            $data = $stock->GetBlock($k);
            // 写入数据库
            if (!empty($data)) {
                $indata = [];
                $dbstock->where('type='.$k)->delete();
                foreach ($data as $values) {
                    if (empty($values[2])) {
                        continue;
                    }
                    $id    = md5($values[1] . $k);
                    if (empty($count)) {
                        $indata[] = [
                            'id'     => $id,
                            'type'   => $k,
                            'name'   => $values[2],
                            'code'   => $values[1],
                            'pinyin' => $values[4],
                            'pycode' => $values[5],
                            'tycode' => $values[3],
                            'uptime' => time()
                        ];
                    } else {
                        if ($count['name'] != $values[2]) {
                            $dbstock->where(['id' => ['EQ', $id]])->save([
                                'type'   => $k,
                                'name'   => $values[2],
                                'code'   => $values[1],
                                'pinyin' => $values[4],
                                'pycode' => $values[5],
                                'tycode' => $values[3],
                                'uptime' => time()
                            ]);
                        }
                    }
                }
                if (!empty($indata)) {
                    echo $k . '插入';
                    var_export($dbstock->addAll($indata));
                    echo PHP_EOL;
                }
            }
        }
    }

    /**
     * 统计预测的准确率
     */
    public function proper(){
        // 对比上证 指数 1A0001

    }

    /**
     * 收益计算
     * 收益=交易收益+股票红利
     * 收益=收益/原始投资总额
     *
     */
    public function proceeds(){
        // 查找持仓

        // 查找当日交易
    }
}
