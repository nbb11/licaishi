<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/10/18
 * Time: 21:06
 */

namespace Home\Controller;


use Think\Controller;
use Org\Net\IpLocation;

class NewsController extends Controller
{
    public function show(){
        $id=I('get.id',0,'intval');
        if(empty($id)){
            echo '新闻id错误';
            exit;
        }
        import('Com.Requests');
        \Requests::register_autoloader();
        $ref = \Requests::get('http://122.144.130.107:8094/cgi-bin/GetStockNewsDetail.ashx?id='.$id);
        $body = $ref->body;
        $body = substr($body,11,-2);
        $body = explode(',',$body);
        $this->assign(['title'=>$body[2],'body'=>$body[4],'time'=>'发布时间：'.$body[3]]);
        $this->display('h5news','utf-8');
    }

    public function reports(){
        $id = I('get.id',0,'intval');
        if(empty($id)){
            exit('失效');
        }
        import('Com.Requests');
        \Requests::register_autoloader();
        $ref = \Requests::get('http://122.144.130.107:8094/cgi-bin/GetReportDetail.ashx?id='.$id);
        $body = $ref->body;
        $body = substr($body,11,-2);
        $body = explode(',',$body);
        $this->assign(['title'=>$body[4],'body'=>$body[6],'time'=>'研究类型：'.$body[1].' 行业：'.$body[3].' 发布时间：'.$body[5]]);
        $this->display('h5news','utf-8');
    }

    /**
     * 有奖活动
     */
    public function activity(){
        $id = I('get.id',0,'intval');
        if(empty($id)){
            exit('无效id');
        }
        $act = M('Activity');
        $data = $act->where(['cid'=>$id])->find();
        $this->assign(['title'=>$data['title'],'body'=>$data['content'],'time'=>'发布时间：'.date('Y-m-d H:i:s',$data['ctime'])]);
        $this->display('h5news','utf-8');
    }

    /**
     * 牛人观点
     */
    public function vpoint(){
        $id = I('get.id',0,'intval');
        if(empty($id)){
            exit('无效id');
        }
        $db        = M('Point');
        $data = $db->where(['id'=>$id])->find();
        $this->assign("info", $data);
        // todo：读取评论列表
        $vlist=M('PointReply')->field(['hpkg_point_reply.rtime','addrip','content','nickname','avatar'])->join('__MEMBER__ ON __MEMBER__.muid=__POINT_REPLY__.muid','LEFT')->where(['mid'=>$id])->order(['hpkg_point_reply.rtime'=>'desc'])->limit(4)->select();
        $ips=new IpLocation('qqwry.dat');
        foreach($vlist as $k=>$v){
            $ip = $ips->getlocation($v['addrip']);
            $v['addrip']=iconv('GBK','UTF-8',$ip['country']);
            $vlist[$k]=$v;
        }
        $this->assign("replist", $vlist);
        $this->display('h5rep','utf-8');
    }
}