<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/10/24
 * Time: 10:05
 */

namespace Home\Controller;


use Think\Controller;
use Org\Net\IpLocation;

class VideoController extends Controller
{
    /**
     * 视频页面
     */
    public function show()
    {
        $id = I('get.id',0,'intval');
        $db = M('Video');
        $info = $db->where(['id'=>$id])->find();
        $info['poster']='Public/Img/noThumbnail.jpg';
        if($info['thumb']==2){
            $info['poster'] = is_file(WEB_ROOT . substr($info['video'], 0, -4) . '_thumb.jpg') ? substr($info['video'], 0,
                    -4) . '_thumb.jpg' : $info['poster'];
        }
        if($info['transform']==2) {
            $info['video'] = is_file(WEB_ROOT . substr($info['video'], 0, -4) . '_new.mp4') ? substr($info['video'], 0,
                    -4) . '_new.mp4' : $info['video'];
        }
        // 拉取评论 评论10条$
        $vlist=M('VideoReply')->field(['hpkg_video_reply.rtime','addrip','content','nickname','avatar'])->join('__MEMBER__ ON __MEMBER__.muid=__VIDEO_REPLY__.muid','LEFT')->where(['mid'=>$id])->order(['hpkg_video_reply.rtime'=>'desc'])->limit(4)->select();
        $ips=new IpLocation('qqwry.dat');
        foreach($vlist as $k=>$v){
            $ip = $ips->getlocation($v['addrip']);
            $v['addrip']=iconv('GBK','UTF-8',$ip['country']);
            $vlist[$k]=$v;
        }
        $this->assign("replist", $vlist);
        $this->assign("info", $info);
        $this->display('stock_index');
    }

    /**
     * 评论列表
     */
    public function relpay(){

    }

    /**
     * 观看记录
     */
    public function watchlog(){

    }
}
