<?php
/**
 * 股票相关页面 手机H5
 */

namespace Home\Controller;


use Think\Controller;

class StockController extends Controller
{
    /**
     * 股票页面
     */
    public function main(){
        // 股票页面
        $code = I('get.code');
        
        if(empty($code)){
            exit('股票id不可为空');
        }
	$arr = split('_',$code);
        if(count($arr) > 1){
                $code = $arr[0];
                $token = $arr[1];

                $member = M('Member');
                $info = $member->where(['muid'=>$token])->find();
        }
        

        $data["code"] = $code;
        // 新闻
        $url = 'http://122.144.130.107:8094/cgi-bin/GetStockNews.ashx?gpdm='.$code;
        $StockNews = str_replace(['var data=["', '"]'], '', file_get_contents($url));
        $StockNews = explode('","', $StockNews);
        foreach ($StockNews as $k=>$val) {
            $val     = explode(',', $val);
            $StockNews[$k] = [
                'author'    => '',
                'stockCode' => $val[1],
                'text'      => $val[2],
                'time'      => $val[3],
                'title'     => $val[2],
                'id'       => $val[0]
            ];
        }
        // 研报
        $url = 'http://122.144.130.107:8094/cgi-bin/GetReportData.ashx?gpdm='.$code;
        $Report = str_replace(['var data=["', '"]'], '', file_get_contents($url));
        $Report = explode('","', $Report);
        foreach ($Report as $k=>$val) {
            $val     = explode(',', $val);
            $Report[$k] = [
                'author'    => $val[5],
                'stockCode' => $val[2],
                'text'      => $val[4],
                'time'      => $val[7],
                'title'     => $val[4],
                'id'       => $val[0]
            ];
        }
        $this->assign('Report',$Report);
        $this->assign('StockNews',$StockNews);
	$this->assign('token',$token);
	$this->assign('info',$info);
        $this->assign($data);
        $this->display('stock_index');
    }

    public function data($id, $type, $jsname){
        header('Content-Type:application/json; charset=utf-8');
        $url = "http://183.136.160.2/EM_HTML5/quote.aspx?id=".substr($id,0,6)."1&type=".$type."&jsname=".$jsname;
        //$ch = curl_init("http://183.136.160.2/EM_HTML5/quote.aspx?id=".$id."&type=".$type."&jsname=".$jsname);
        //curl_setopt($ch, )
        //echo $jsname . "(" . json_encode($data) . ");";
        $data = file_get_contents($url);
        if($data == $jsname){
            $url = "http://183.136.160.2/EM_HTML5/quote.aspx?id=".substr($id,0,6)."2&type=".$type."&jsname=".$jsname;
            $data1 = file_get_contents($url);
            echo $data1;
        }else {
            echo $data;
        }
    }

    public function data2($id){
        //header('charset=utf-8');
        $url = "http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=P.[(x)]|".$id."1&sty=MPICT&st=z&sr=&p=&ps=&cb=callback&js=&token=aaf32a88de888ea7b0ea63e017ecb049&_=1447335108373";
        $data = file_get_contents($url);
        if($data == "callback([[]])"){
            $url = "http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=P.[(x)]|".$id."2&sty=MPICT&st=z&sr=&p=&ps=&cb=callback&js=&token=aaf32a88de888ea7b0ea63e017ecb049&_=1447335108373";
            $data1 = file_get_contents($url);
            echo $data1;
        }else {
            echo $data;
        }
    }

    /**
     * 大盘预测
     */
    public function grail(){
        // 读取预测统计
        // 读取当天统计
        $dbmod = M('MarketForecastLog');
        $time = date('Y-m-d');
        $data=$dbmod->field('type,count(id) as total')->where(['time'=>$time])->group('type')->select();
        foreach($data as $k=>$v){
            $datas[$v['type']]=$v['total'];
        }
        $total = array_sum($datas);
        foreach($datas as $k=>$v){
            $v=['total'=>$v,'be'=>(empty($total)?0:(round($v/$total,4)*100))];
            $datas[$k]=$v;
        }
        for($i=1;$i<4;$i++){
            if(!isset($datas[$i])){
                $datas[$i]=['total'=>0,'be'=>0];
            }
        }
        $this->assign(['data'=>$datas]);
        $this->display('grail');
    }

    /**
     * 后期添加验证
     */
    public function pgrail(){
        $uid = 1;
        $type = I('post.type',0,'intval');
        if(!in_array($type,[1,2,3])){
            $this->ajaxReturn(['code'=>__LINE__,'msg'=>'无效type']);
        }
        $time = date('Y-m-d');
        $id = md5($uid.$time);
        $dbmod = M('MarketForecastLog');
        $data=$dbmod->where(['id'=>$id])->count();
        if(!empty($data)){
            $this->ajaxReturn(['code'=>__LINE__,'msg'=>'今日你已经投过票']);
        }
        $rep=$dbmod->data(['id'=>$id,'muid'=>$uid,'type'=>$type,'time'=>$time])->add();
        if($rep){
            $this->ajaxReturn(['code'=>0,'msg'=>'投票成功']);
        }else{
            $this->ajaxReturn(['code'=>__LINE__,'msg'=>'投票失败']);
        }
    }
}
