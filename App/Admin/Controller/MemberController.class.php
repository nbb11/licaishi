<?php
/**
 * 会员管理
 * User: shaoqi123@gmail.com
 * Date: 2015/10/15
 * Time: 23:40
 */

namespace Admin\Controller;


use Common\Controller\CommonController;

class MemberController extends CommonController
{
    /*
     * 普通会员列表
     */
    public function index()
    {
        $where       = [];
        $search      = I('post.search', []);
        $pageSize    = I('post.pageSize', 20, 'intval');
        $pageCurrent = I('post.pageCurrent', 1, 'intval');
        if (!empty($search['regdate'])) {
            $where['rtime'] = ['between', [strtotime($search['regdate']), strtotime($search['regdate'] . ' 23:59:59')]];
        }
        if (!empty($search['nickname'])) {
            $where['nickname'] = ['like', '%' . trim($search['nickname']) . '%'];
        }
        $where['type'] = 0;
        $db            = M('Member');
        $count         = $db->where($where)->count();
        $list          = $db->field([
            'muid',
            'bankroll',
            'locks',
            'phone',
            'nickname',
            'sex',
            'rtime',
            'subscribe',
            'follow',
            'sharings',
            'standpoint',
            'interlocution'
        ])->join('__MEMBER_COUNT__ ON __MEMBER__.muid=__MEMBER_COUNT__.mid',
            'LEFT')->where($where)->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();
        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('list', $list);
        $this->display();
    }

    /**
     * 密码设置
     */
    public function setpwd()
    {
        // 重置成功后发送短信
        $db  = M('Member');
        $uid = I('get.uid', 0, 'intval');
        if (empty($uid)) {
            $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
        }
        $pwd     = rand(100000, 999999);
        $hashpwd = mkpwd($pwd);
        $rep     = $db->where(['muid' => $uid])->setField('pwd', $hashpwd);
        if ($rep) {
            $this->ajaxReturn(['statusCode' => 200, 'message' => '密码重置成功']);
        } else {
            $this->ajaxReturn(['statusCode' => 300, 'message' => '操作失败']);
        }
    }

    /**
     * 锁定会员
     */
    public function setlock()
    {
        $db  = M('Member');
        $uid = I('get.uid', 0, 'intval');
        if (empty($uid)) {
            $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
        }
        $lock = $db->getFieldByMuid($uid, 'locks');
        $rep  = $db->where(['muid' => $uid])->setField('locks', (1 - $lock));
        if ($rep) {
            $this->ajaxReturn(['statusCode' => 200, 'message' => ($lock ? '解锁' : '锁定') . '成功']);
        } else {
            $this->ajaxReturn(['statusCode' => 300, 'message' => '操作失败']);
        }

    }

    /**
     * 牛人审核
     */
    public function verify()
    {
        $db    = M('Planner');
        if (IS_POST) {
            $muid = I('post.uid');
            $data = $db->where("muid = '{$muid}'")->find();
            $state = 1 - intval($data['state']);
            $res = $db->where("muid = '{$muid}'")->setField('state',$state);
            $this->ajaxReturn(['statusCode' => 200, 'message' => '操作成功']);
        } else {
            $uid = I('get.uid','0','intval');
            if(empty($uid)){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
            }
            $info=$db->where(['muid'=>$uid])->find();
            $this->assign('info', $info);
            $this->assign('title', '查看用户ID:[' . $uid . ']的牛人申请');
            $this->display();
        }
    }

    /**
     * 牛人列表
     */
    public function planner()
    {
        $where           = [];
        $search          = I('post.search', []);
        $pageSize        = I('post.pageSize', 20, 'intval');
        $pageCurrent     = I('post.pageCurrent', 1, 'intval');
        $pageCurrent     = max(1, $pageCurrent);
        $search['state'] = ($search['state'] === '' || !isset($search['state'])) ? -1 : $search['state'];
        if (!empty($search['regdate'])) {
            $where['rtime'] = ['between', [strtotime($search['regdate']), strtotime($search['regdate'] . ' 23:59:59')]];
        }
        if (!empty($search['nickname'])) {
            $where['nickname'] = ['like', $search['nickname'] . '%'];
        }
        if (!empty($search['phone']) && preg_match('/^0?(13|15|18|14|17)[0-9]{9}$/',$search['phone'])) {
            $where['phone'] = $search['phone'];
        }else{
            $search['phone']='';
        }
        // 申请时间
        if (!empty($search['rptime'])) {
            $where['rptime'] = ['between', [strtotime($search['rptime']), strtotime($search['rptime'] . ' 23:59:59')]];
        }
        // 审核状态
        if ($search['state'] != -1) {
            $where['state'] = $search['state'];
        }
        $db    = M('Planner');
        $count = $db->join('__MEMBER__ ON __MEMBER__.muid=__PLANNER__.muid', 'LEFT')->where($where)->count();
        $list  = $db->field([
            'hpkg_planner.muid',
            'state',
            'rptime',
            'bankroll',
            'locks',
            'phone',
            'nickname',
            'sex',
            'rtime',
            'subscribe',
            'follow',
            'sharings',
            'standpoint',
            'interlocution'
        ])->join('__MEMBER__ ON __MEMBER__.muid=__PLANNER__.muid',
            'LEFT')->join('__MEMBER_COUNT__ ON __PLANNER__.muid=__MEMBER_COUNT__.mid',
            'LEFT')->where($where)->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->order('muid asc')->select();
        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('list', $list);
        $this->display();
    }

    /**
     * 发送消息
     */
    public function sendmsg()
    {
        if (IS_POST) {
            $msg   = I('post.msg');
            $title = I('post.title');
            $uid   = I('post.uid', 0, 'trim');
            // 是否推送
            $push = I('post.ispush', 0, 'intval');
            // 是否站内消息
            $ismsg = I('post.ismsg', 0, 'intval');
            $this->ajaxReturn(['statusCode' => 200, 'message' => '消息发送成功']);
        } else {
            $uid = I('get.uid', 'post.uid', 'trim');
            if (empty($uid)) {
                $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
            }
            $this->assign('uid', $uid);
            $this->assign('title', '向用户ID:[' . $uid . ']发送信息');
            $this->display();
        }
    }

    /**
     * 转为牛人F_dw
     */
    public function opplaner() {
        $muid = I('get.uid');

        $data_p['muid'] = $muid;
        $data_p['state'] = 0;
        $data_p['rptime'] = time();

        $data_mc['mid'] = $muid;

        $opstate = M('Member')->where("muid = '{$muid}'")->setField('type',1);
        if(!is_bool($opstate)) {
            $planner_data = M('Planner')->add($data_p);
            if($planner_data) {
                $mc_data = M('Member_count')->add($data_mc);
            }
            $this->ajaxReturn(['statusCode' => 200, 'message' => '操作成功']);
        }
    }

    /**
     * 当前持仓列表F_dw
     */
    public function currpos() {
        $where=[];
        //$search['trtime'] = I('post.trtime',0,'trim') ? I('post.trtime',0,'trim') : '';
        $search['code']   = I('post.code',0,'trim') ? I('post.code',0,'trim') : '';
        $search['muid']   = I('post.muid',0,'intval') ? I('post.muid',0,'intval') : '';
        $pageSize          = I('post.pageSize', 20, 'intval');
        $pageCurrent       = I('post.pageCurrent', 1, 'intval');
        $pageCurrent       = max(1, $pageCurrent);

        if(!empty($search['trtime'])){
            $where['trtime']=['between', [strtotime($search['trtime']), strtotime($search['trtime'] . ' 23:59:59')]];;
        }
        if(!empty($search['code'])) {
            $where['code'] = $search['code'];
        }
        if(!empty($search['muid'])) {
            $where['muid'] = $search['muid'];
        }
        if(I('get.uid')) {
            $where['muid'] = I('get.uid');
            $search['muid'] = I('get.uid');
        }

        $count = M('stock_position')->where($where)->count();
        $data  = M('stock_position')->where($where)->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->order('atime DESC,ctime DESC')->select();
        foreach($data as $key => $v) {
            $data[$key]['codename'] = M('stock')->where("code = '{$v[code]}'")->getField('name');
            $data[$key]['nickname'] = M('member')->where("muid = '{$v[muid]}'")->getField('nickname');
        }
        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('data',$data);
        $this->display();
        
    }

    /**
     * 模拟交易列表F_dw
     */
    public function simtra() {
        $where=[];
        $search['trtime'] = I('post.trtime',0,'trim') ? I('post.trtime',0,'trim') : '';
        $search['code']   = I('post.code',0,'trim') ? I('post.code',0,'trim') : '';
        $search['muid']   = I('post.muid',0,'intval') ? I('post.muid',0,'intval') : '';
        $pageSize          = I('post.pageSize', 20, 'intval');
        $pageCurrent       = I('post.pageCurrent', 1, 'intval');
        $pageCurrent       = max(1, $pageCurrent);

        if(!empty($search['trtime'])){
            $where['trtime']=['between', [strtotime($search['trtime']), strtotime($search['trtime'] . ' 23:59:59')]];;
        }
        if(!empty($search['code'])) {
            $where['code'] = $search['code'];
        }
        if(!empty($search['muid'])) {
            $where['muid'] = $search['muid'];
        }
        if(I('get.uid')) {
            $where['muid'] = I('get.uid');
            $search['muid'] = I('get.uid');
        }
        if(I('get.code')) {
            $where['code'] = I('get.code');
            $search['code'] = I('get.code');
        }

        $count = M('stock_trades_log')->where($where)->count();
        $data  = M('stock_trades_log')->where($where)->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->order('trtime DESC')->select();
        foreach($data as $key => $v) {
            $data[$key]['codename'] = M('stock')->where("code = '{$v[code]}'")->getField('name');
            switch($v['status']) {
                case 0:
                    $data[$key]['status'] = '完成交易';
                    break;
                case 1:
                    $data[$key]['status'] = '委托';
                    break;
                case 2:
                    $data[$key]['status'] = '撤单';
                    break;
            }
            $data[$key]['type'] = $v['type'] == 1 ? '卖出' : '买入';
        }
        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('data',$data);
        $this->display();
        
    }

    /**
     * 模拟交易编辑F_dw
     */
    public function simtraed() {
        $id = I('get.id');
        $data['present'] = I('get.present');
        $data['cost'] = I('get.cost');
        $data['price'] = I('get.price');
        $res = M('stock_trades_log')->where("id = '{$id}'")->save($data);
        if($res) {
            $this->ajaxReturn(array('statusCode' => 200, 'message' => "ok"));
        } else {
            $this->ajaxReturn(array('statusCode' => 300, 'message' => "no"));
        }
    }

    /**
     * 当前持仓编辑F_dw
     */
    public function currposed() {
        $sid = I('get.sid');
        $data['price'] = I('get.prices');
        $res = M('stock_position')->where("sid = '{$sid}'")->save($data);
        if($res) {
            $this->ajaxReturn(array('statusCode' => 200, 'message' => "ok"));
        } else {
            $this->ajaxReturn(array('statusCode' => 300, 'message' => "no"));
        }
    }

}