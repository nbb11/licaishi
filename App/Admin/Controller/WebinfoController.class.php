<?php

/**
 * @author 小策一喋 <xvpindex@qq.com>
 * @link http://www.topstack.cn
 * @copyright Copyright (C) 2014 EWSD
 * @datetime 2014-10-15 12:40
 * @version 1.0
 * @description
 */

namespace Admin\Controller;

use Think\Controller;
use Common\Controller\CommonController;
use Common\Model\BaseModel;
use Common\Model\CommonModel;

// 本类设置项目一些常用信息
class WebinfoController extends CommonController {

    /**
      +----------------------------------------------------------
     * 配置网站信息
      +----------------------------------------------------------
     */
    public function index2() {
        $this->checkSystemConfig();
    }

    public function index() {
        if (IS_POST) {
            $id = I('post.id');
            $type = I('post.type');
            $name = I('post.name');
            $code = I('post.code');
            $value = I('post.value');
            $desc = I('post.desc');
            $M = M('Config');
            foreach ($id as $k => $v) {
                $data['id'] = $v;
                $data['type'] = $type[$k];
                $data['name'] = $name[$k];
                $data['code'] = $code[$k];
                $data['value'] = $value[$k];
                $data['desc'] = $desc[$k];
                $result = $M->save($data);
            }
            if($result) {
                CommonModel::writeSiteConfig();
                $this->ajaxReturn(array('statusCode' => 200, 'message' => '更新成功'));
            } else {
                $this->ajaxReturn(array('statusCode' => 300, 'message' => '更新失败'));
            }
        } else {
            $list = BaseModel::getPageList($param = array('modelName' => 'Config', 'field' => '*', 'order' => 'id ASC', 'listRows' => '10'), $condition = '');
            $this->assign('list', $list['info']);
            $this->assign('page', $list['page']);
            $this->display();
        }
    }

    /**
      +----------------------------------------------------------
     * 配置网站邮箱信息
      +----------------------------------------------------------
     */
    public function setEmailConfig() {
        $this->checkSystemConfig("SYSTEM_EMAIL");
    }

    /**
      +----------------------------------------------------------
     * 配置网站信息
      +----------------------------------------------------------
     */
    public function setSafeConfig() {
        $this->checkSystemConfig("TOKEN");
    }

    /**
      +----------------------------------------------------------
     * 网站配置信息保存操作等
      +----------------------------------------------------------
     */
    private function checkSystemConfig($obj = "SITE_INFO") {
        if (IS_POST) {
            $this->checkToken();
            $config = APP_PATH . "Common/Conf/config_site.php";
            $config = file_exists($config) ? include "$config" : array();
            $config = is_array($config) ? $config : array();
            $config = array_merge($config, array("$obj" => $_POST));
            $str = $obj == "SITE_INFO" ? "网站配置信息" : $obj == "SYSTEM_EMAIL" ? "系统邮箱配置" : "安全设置";
            //if (F("config_site", $config, APP_PATH . "Common/Conf/")) {
            $content = '<?php return ' . var_export($config, true) . ';';
            if(file_put_contents(APP_PATH . "Common/Conf/config_site.php", $content)) {
                delDirAndFile(WEB_CACHE_PATH . "Runtime/Admin/~runtime.php");
                if ($obj == "TOKEN") {
                    unset($_SESSION, $_COOKIE);
                    $this->ajaxReturn(array('statusCode' => 200, 'message' => $str . '已更新，你需要重新登录', 'url' => __APP__ . '?' . time()));
                } else {
                    $this->ajaxReturn(array('statusCode' => 200, 'message' => $str . '已更新'));
                }
            } else {
                $this->ajaxReturn(array('statusCode' => 300, 'message' => $str . '失败，请检查', 'url' => __ACTION__));
            }
        } else {
            $this->display();
        }
    }

    /**
      +----------------------------------------------------------
     * 测试邮件账号是否配置正确
      +----------------------------------------------------------
     */
    public function testEmailConfig() {
        C('TOKEN_ON', false);
        $return = send_mail($_POST['test_email'], "", "测试配置是否正确", "这是一封测试邮件，如果收到了说明配置没有问题", "", $_POST);
        if ($return == 1) {
            $this->ajaxReturn(array('statusCode' => 200, 'message' => "测试邮件已经发往你的邮箱" . $_POST['test_email'] . "中，请注意查收"));
        } else {
            $this->ajaxReturn(array('statusCode' => 300, 'message' => "$return"));
        }
    }


    public function bootstrap(){
        $where=[];
        $search=I('post.search',[]);
        $pageSize = I('post.pageSize',20,'intval');
        $pageCurrent = I('post.pageCurrent',1,'intval');
        $pageCurrent = max(1,$pageCurrent);
        if(!empty($search['atime'])){
            $where['atime']=['between',[strtotime($search['atime']),strtotime($search['atime'].' 23:59:59')]];
        }
        $where['type']=1;
        $db = M('Banner');
        $count= $db->where($where)->count();
        $list= $db->where($where)->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();
        $this->assign('search',$search);
        $this->assign('total',$count);
        $this->assign('list',$list);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $data=[];
            $data['imgs'] = I('post.imgs','','trim');
            if(empty($data['imgs'])){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
            }
            $data['atime']=time();
            $data['type']=1;
            $db = M('Banner');
            $ref=$db->data($data)->add();
            if(empty($ref)){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '保存失败']);
            }else{
                $this->ajaxReturn(['statusCode' => 200, 'message' => 'banner设置成功']);
            }
        }else {
            $this->display();
        }
    }

    /**
     * 编辑开机动画F_dw
     */
    public function edit(){
        if(IS_POST){
            $data=[];
            $data['imgs'] = I('post.imgs','','trim');
            if(empty($data['imgs'])){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
            }
            //$data['atime']=time();
            $data['type']=1;
            $bid = I('post.bid');
            $db = M('Banner');
            $ref=$db->where("bid = '{$bid}'")->save($data);
            if(empty($ref)){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '保存失败']);
            }else{
                $this->ajaxReturn(['statusCode' => 200, 'message' => 'banner设置成功']);
            }
        }else {
            $bid = I('get.id');
            $data = M('Banner')->where("bid = '{$bid}'")->find();
            $this->assign('data',$data);
            $this->display();
        }
    }


    /**
     * 删除视频
     */
    public function del()
    {
        $id = I('get.bid','0','intval');
        $ids = I('get.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $db = M('Banner');
        $del = $db->where(['bid'=>['in',$ids]])->delete();
        if($del){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '删除成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '删除失败']);
        }
    }

    public function upload(){
        $upload           = new \Think\Upload();
        $upload->exts     = array('jpg', 'gif', 'png', 'jpeg');
        $upload->hash     = true;
        $upload->rootPath = WEB_ROOT;
        $upload->savePath = 'Uploads' . DIRECTORY_SEPARATOR . 'banner' . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m') . DIRECTORY_SEPARATOR . date('d') . DIRECTORY_SEPARATOR;
        $upload->replace  = false;
        $upload->autoSub  = true;
        $info             = $upload->uploadOne($_FILES['file']);
        if (empty($info)) {
            $this->ajaxReturn(['statusCode' => 300, 'message' => $upload->getError()]);
        }
        $file = str_replace('\\','/',$info['savepath']).$info['savename'];
        $url = U($file, '', '', true);
        $this->ajaxReturn(['statusCode' => 200, 'message' => '上传成功','filename'=>$file]);
    }

}

?>