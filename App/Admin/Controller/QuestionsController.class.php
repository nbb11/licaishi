<?php
/**
 * 问答管理
 * Date: 2015/10/22
 * Time: 20:53
 */

namespace Admin\Controller;


use Common\Controller\CommonController;

class QuestionsController extends CommonController
{
    /**
     * 列表
     */
    public function index(){
        $search['qtime'] = I('post.qtime');
        $search['atime'] = I('post.atime');
        $search['mnickname'] = I('post.mnickname');
        $search['uname'] = I('post.uname');
        $pageSize        = I('post.pageSize', 20, 'intval');
        $pageCurrent     = I('post.pageCurrent', 1, 'intval');
        $pageCurrent     = max(1, $pageCurrent);
        if (!empty($search['qtime'])) {
            $where['qtime'] = ['between', [strtotime($search['qtime']), strtotime($search['qtime'] . ' 23:59:59')]];
        }
        if (!empty($search['atime'])) {
            $where['atime'] = ['between', [strtotime($search['atime']), strtotime($search['atime'] . ' 23:59:59')]];
        }
        if (!empty($search['mnickname'])) {
            $where['m.nickname'] = array('like', "%".trim($search['mnickname'])."%");
        }
        if (!empty($search['uname'])) {
            $where['u.name'] = array('like', "%".trim($search['uname'])."%");
        }
        $m = M('qa');
        //@author-tc
        $count = $m->alias('q')->join("left join ".C("DB_PREFIX")."member m on m.muid = q.quid")->join("left join ".C("DB_PREFIX")."user u on u.uid = q.aid")->field("q.*, u.name as uname, m.nickname as mnickname, m.phone as mphone")->where($where)->count();

        $list  = $m->alias('q')->join("left join ".C("DB_PREFIX")."member m on m.muid = q.quid")->join("left join ".C("DB_PREFIX")."user u on u.uid = q.aid")->field("q.*, u.name as uname, m.nickname as mnickname, m.phone as mphone")->where($where)->order("atime DESC")->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();
        //@author-tc

        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('list', $list);
        $this->display();
    }

    //@author-tc
    public function isPass() {
        $isPass = I('get.isPass',0,'intval');
        $id = I('get.id',0,'intval');
        if($isPass) {
            $isPass = 0;
            $info = "已取消审核！";
        } else {
            $isPass = 1;
            $info = "已审核！";
        }

        $result = M('qa')->where("id = '{$id}'")->setField('isPass',$isPass);
        if(!is_bool($result)) {
            $this->ajaxReturn(array('statusCode' => 200, 'message' => $info));
        } else {
            $this->ajaxReturn(array('statusCode' => 300, 'message' => '操作失败，请检查！'));
        }
    }

    public function info() {
        $id = I('get.id');
        $info = M('qa')->alias('q')->join("left join ".C("DB_PREFIX")."member m on m.muid = q.quid")->join("left join ".C("DB_PREFIX")."user u on u.uid = q.aid")->where("q.id = '".$id."'")->field("q.*, u.name as uname, m.nickname as mnickname, m.phone as mphone")->find();
        
        $this->assign('info', $info);
        $this->display();
    }
    //@author-tc

    /**
     * 删除问答F_dw
     */
    public function del()
    {
        $id = I('get.id','0','intval');
        $ids = I('get.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $del = M('qa')->where(['id'=>['in',$ids]])->delete();
        if(!is_bool($del)){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '删除成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '删除失败']);
        }
    }

}