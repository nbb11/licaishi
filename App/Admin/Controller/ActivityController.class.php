<?php

/**
 * @author phpboy <admin@shaoqi.net>
 */

namespace Admin\Controller;

use Think\Controller;
use Common\Controller\CommonController;

class ActivityController extends CommonController {

    /**
      +----------------------------------------------------------
     * 定义
      +----------------------------------------------------------
     */
    protected $model;

    /**
      +----------------------------------------------------------
     * 初始化
      +----------------------------------------------------------
     */
    public function _initialize() {
        parent::_initialize();
        $this->model = M('Activity');
    }

    /**
      +----------------------------------------------------------
     * 列表
      +----------------------------------------------------------
     */
    public function index() {
        $search           = I('post.search', []);
        $pageSize         = I('post.pageSize', 20, 'intval');
        $pageCurrent      = I('post.pageCurrent', 1, 'intval');
        $pageCurrent      = max(1, $pageCurrent);
        if (!empty($search['ctime'])) {
            $where['ctime'] = ['between', [strtotime($search['ctime']), strtotime($search['ctime'] . ' 23:59:59')]];
        }
        if (!empty($search['title'])) {
            $where['title'] = ['like', '%' . trim($search['title']) . '%'];
        }
        $count = $this->model->where($where)->count();
        $list  = $this->model->where($where)->order("isTop DESC,ctime DESC")->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();//@author-tc //添加order

        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('list', $list);
        $this->display();
    }

    /**
      +----------------------------------------------------------
     * 新增
      +----------------------------------------------------------
     */
    public function add() {
        if (IS_POST) {
            $data = I('post.info');
            //@author-tc
            $only_title = $this->model->where("title = '{$data['title']}'")->find();
            if($only_title) {
                $this->ajaxReturn(['statusCode' => 300, 'message' => '标题已经存在！']);
            }
            if($data['optime'] > $data['endtime']) {
                $this->ajaxReturn(['statusCode' => 300, 'message' => '起止时间的开始时间不应大于结束时间！']);
            }//@author-tc
            $Image = new \Com\Image();
            $data['content'] = $Image->getImageToLocal($data['content']);
            if($data['summary'] == ""){
                $data['summary'] = cutStr($data['content'],150);
            }
            //@author-tc
            $data['ctime'] = time();
            if($data['optime'] == $data['endtime']){
                $data['endtime'] = strtotime($data['endtime'].' 23:59:59');
            }else{
                $data['endtime'] = strtotime($data['endtime']);
            }
            $data['optime'] = strtotime($data['optime']);
            //@author-tc
            $ref = $this->model->data($data)->add();
            if($ref){
                $this->ajaxReturn(['statusCode'=>200,'message'=>'活动创建成功','closeCurrent'=>true]);
            }else{
                $this->ajaxReturn(['statusCode'=>300,'message'=>'活动创建异常']);    
            }
        } else {
            $this->display();
        }
    }

    /**
      +----------------------------------------------------------
     * 编辑
      +----------------------------------------------------------
     */
    public function edit() {
        if (IS_POST) {
            $data = I('post.info');

            $Image = new \Com\Image();
            $data['content'] = $Image->getImageToLocal($data['content']);
            if($data['summary'] == ""){
                $data['summary'] = cutStr($data['content'],150);
            }
            //@author-tc
            if($data['optime'] == $data['endtime']){
                $data['endtime'] = strtotime($data['endtime'].' 23:59:59');
            }else{
                $data['endtime'] = strtotime($data['endtime']);
            }
            $data['optime'] = strtotime($data['optime']);
            $cid = $data['id'];
            unset($data['id']);
            $only_title = $this->model->where("title = '{$data['title']}'")->find();
            if($only_title['cid'] && $only_title['cid'] != $cid) {
                $this->ajaxReturn(['statusCode' => 300, 'message' => '标题已经存在！']);
            }
            if($data['optime'] > $data['endtime']) {
                $this->ajaxReturn(['statusCode' => 300, 'message' => '起止时间的开始时间不应大于结束时间！']);
            }
            
            $ref = $this->model->data($data)->where(['cid'=>$cid])->save();
            if(is_bool($ref)){
                $this->ajaxReturn(['statusCode'=>300,'message'=>'活动编辑异常']);
            }else{
                $this->ajaxReturn(['statusCode'=>200,'message'=>'活动编辑成功','closeCurrent'=>true]);
            }
            //@author-tc
        } else {
            $condition = "cid=" . I('get.id', 0, 'intval');
            $info=$this->model->where($condition)->find();
            $info['endtime']=date('Y-m-d',$info['endtime']);
            $info['optime']=date('Y-m-d',$info['optime']);
            $this->assign("info", $info);
            $this->display("add");
        }
    }

    /**
      +----------------------------------------------------------
     * 移除
      +----------------------------------------------------------
     */
    public function remove() {
        $id = I('get.id','0','intval');
        $ids = I('get.ids','','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $del = $this->model->where(['cid'=>['in',$ids]])->delete();
        if($del){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '删除成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '删除失败']);
        }
    }

    public function checkArticleTitle() {
        $M = M("Activity");
        $where = "title='" . $_GET['title'] . "'";
        if (!empty($_GET['id'])) {
            $where.=" And cid !=" . (int) $_GET['id'];
        }
        if ($M->where($where)->count() > 0) {
            $this->ajaxReturn(array('statusCode' => 300, 'message' => "已经存在，请修改标题"));
        } else {
            $this->ajaxReturn(array('statusCode' => 200, 'message' => "可以使用"));
        }
    }

    //@author-tc
    public function isTop() {
        $isTop = I('get.isTop',0,'intval');
        $cid = I('get.cid',0,'intval');
        if($isTop) {
            $isTop = 0;
            $info = "取消置顶！";
        } else {
            $isTop = 1;
            $info = "已置顶！";
        }

        $result = M('Activity')->where("cid = '{$cid}'")->setField('isTop',$isTop);
        if(!is_bool($result)) {
            $this->ajaxReturn(array('statusCode' => 200, 'message' => $info));
        } else {
            $this->ajaxReturn(array('statusCode' => 300, 'message' => '操作失败，请检查！'));
        }
    }
    //@author-tc

}