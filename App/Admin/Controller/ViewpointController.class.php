<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/10/26
 * Time: 2:44
 */

namespace Admin\Controller;


use Common\Controller\CommonController;

class ViewpointController extends CommonController
{
    /**
     * +----------------------------------------------------------
     * 定义
     * +----------------------------------------------------------
     */
    protected $model;

    /**
     * +----------------------------------------------------------
     * 初始化
     * +----------------------------------------------------------
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->model = M('Point');
    }

    public function index(){
        $where            = [];
        $search           = I('post.search', []);
        $pageSize         = I('post.pageSize', 20, 'intval');
        $pageCurrent      = I('post.pageCurrent', 1, 'intval');
        $pageCurrent      = max(1, $pageCurrent);
        $search['status'] = isset($search['status']) ? ($search['status'] === '' ? -1 : $search['status']) : -1;
        if (!empty($search['atime'])) {
            $where['atime'] = ['between', [strtotime($search['atime']), strtotime($search['atime'] . ' 23:59:59')]];
        }
        if (!empty($search['title'])) {
            $where['title'] = ['like', $search['title'] . '%'];
        }
        if ($search['status'] != -1) {
            $where['status'] = $search['status'];
        }
        if (!empty($search['type'])) {
            $where['type'] = $search['type'];
        }
        $count = $this->model->where($where)->count();
        $list  = $this->model->where($where)->order('atime desc')->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();
        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('list', $list);
        $this->display();
    }

    /**
     * 批量删除
     */
    public function del(){
        $id = I('get.id','0','intval');
        $ids = I('get.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $del = $this->model->where(['id'=>['in',$ids]])->delete();
        if($del){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '处理成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '处理失败']);
        }
    }

    /**
     * 评论删除
     */
    public function rdel(){
        $id = I('get.id','0','trim');
        $ids = I('post.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $dbrep=M('PointReply');
        $del = $dbrep->where(['rid'=>['in',$ids]])->delete();
        if($del){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '处理成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '处理失败']);
        }
    }

    public function info(){
        $id = I('get.id', 0 , 'intval');
        if(empty($id)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $info = $this->model->where(['id'=>$id])->find();
        $this->assign("info", $info);
        $this->display();
    }

    /**
     * 置顶
     */
    public function stick(){
        $id = I('get.id','0','intval');
        $ids = I('post.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $del = $this->model->execute('update __POINT__ set `ishot`=1-`ishot` where id in (\''.implode('\',\'',$ids).'\')');
        if($del){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '处理成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '处理失败']);
        }
    }

    /**
     * 批量审核
     */
    public function change(){
        $id = I('get.id','0','intval');
        $ids = I('post.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $del = $this->model->execute('update __POINT__ set `status`=1-`status` where id in (\''.implode('\',\'',$ids).'\')');
        if($del){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '处理成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '处理失败']);
        }
    }

    /**
     * 回复列表
     */
    public function replist(){
        $where=[];
        $search=[];
        $search['mid']=I('get.id',0,'intval');
        $search['rtime']=I('get.rtime','','trim');
        $search['content']=I('get.content','','trim');
        $pageSize         = I('post.pageSize', 20, 'intval');
        $pageCurrent      = I('post.pageCurrent', 1, 'intval');
        $pageCurrent      = max(1, $pageCurrent);
        if(!empty($search['mid'])){
            $where['mid']=$search['mid'];
        }
        if(!empty($search['rtime'])){
            $where['rtime']=['between', [strtotime($search['rtime']), strtotime($search['rtime'] . ' 23:59:59')]];;
        }
        if (!empty($search['content'])) {
            $where['content'] = ['like', trim($search['content']) . '%'];
        }
        $dbrep=M('PointReply');
        //@author-tc
        $count = $dbrep->alias("p_r")->join("left join ".C("DB_PREFIX")."member m on m.muid = p_r.muid")->join("left join ".C("DB_PREFIX")."point p on p.id = p_r.mid")->where($where)->count();

        $list  = $dbrep->alias("p_r")->join("left join ".C("DB_PREFIX")."member m on m.muid = p_r.muid")->join("left join ".C("DB_PREFIX")."point p on p.id = p_r.mid")->where($where)->field("p_r.*,m.nickname as mnickname,p.title as ptitle")->order('rtime desc')->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();
        //@author-tc
        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('list', $list);
        $this->display();
    }
}