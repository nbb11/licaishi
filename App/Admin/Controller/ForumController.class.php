<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/10/26
 * Time: 20:17
 */

namespace Admin\Controller;


use Home\Controller\CommonController;

class ForumController extends CommonController
{
    // 主题管理 F_dw update:phpboy
    public function index(){
        $where=[];
        $search = I('post.search');
        $search['regdate'] = I('post.regdate',0,'trim') ? I('post.regdate',0,'trim') : '';
        $search['title']   = I('post.title',0,'trim') ? I('post.title',0,'trim') : '';
        $pageSize          = I('post.pageSize', 20, 'intval');
        $pageCurrent       = I('post.pageCurrent', 1, 'intval');
        $pageCurrent       = max(1, $pageCurrent);

        if(!empty($search['regdate'])){
            $where['dateline']=['between', [strtotime($search['regdate']), strtotime($search['regdate'] . ' 23:59:59')]];;
        }
        if (!empty($search['title'])) {
            $where['subject'] = ['like', '%' .$search['title']. '%'];
        }
        $dbrep=M('ForumThread');

        $count = $dbrep->where($where)->count();
        $list  = $dbrep->where($where)->order("tid desc")->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();

        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('list', $list);
        $this->display();
    }

    /**
     * 帖子删除F_dw
     */
    public function dels(){
        $id = I('get.id','0','intval');
        $ids = I('get.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        // 检索主题id
        /*$fids = M('ForumPost')->field('tid')->where(['pid'=>['in',$ids],'first'=>1])->select();
        foreach($fids as $k=>$val){
            $fids[$k]=$val['tid'];
        }*/
        $del = M('ForumPost')->where(['pid'=>['in',$ids]])->delete();
        if($del){
            /*if(!empty($fids)){
                $del = M('ForumThread')->where(['tid'=>['in',$ids]])->delete();
                $del = M('ForumPost')->where(['tid'=>['in',$ids]])->delete();
            }*/
            $this->ajaxReturn(['statusCode' => 200, 'message' => '删除成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '删除失败']);
        }
    }

    /**
     * 主题删除
     */
    public function fdel(){
        $id = I('get.id','0','intval');
        $ids = I('get.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $del = M('ForumThread')->where(['tid'=>['in',$ids]])->delete();
        $delp = M('ForumPost')->where(['tid'=>['in',$ids]])->delete();
        $this->ajaxReturn(['statusCode' => 200, 'message' => '删除成功']);
    }

    /**
     * 回复列表F_dw
     */
    public function posts(){
        $where=[];
        $search = I('post.search');
        $search['dateline'] = I('post.dateline',0,'trim') ? I('post.dateline',0,'trim') : '';
        $search['author']   = I('post.author',0,'trim') ? I('post.author',0,'trim') : '';
        $pageSize         = I('post.pageSize', 20, 'intval');
        $pageCurrent      = I('post.pageCurrent', 1, 'intval');
        $pageCurrent      = max(1, $pageCurrent);
        if(!empty($search['dateline'])){
            $where['dateline']=['between', [strtotime($search['dateline']), strtotime($search['dateline'] . ' 23:59:59')]];;
        } else {
            $search['dateline'] = '';
        }
        if(!empty($search['author'])){
            $where['author']=trim($search['author']);
        } else {
            $search['author'] = '';
        }
        $dbrep=M('ForumPost');
        if(I('get.tid')) { $where['tid'] = I('get.tid'); }
        
        $count = $dbrep->where($where)->count();
        $list  = $dbrep->where($where)->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();
        $this->assign('search', $search);
        $this->assign('total', $count);
        $this->assign('list', $list);
        $this->display();
    }
}