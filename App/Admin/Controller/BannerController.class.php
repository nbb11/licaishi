<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/10/26
 * Time: 0:45
 */

namespace Admin\Controller;


use Common\Controller\CommonController;

class BannerController extends CommonController
{
    /**
    +----------------------------------------------------------
     * 定义
    +----------------------------------------------------------
     */
    protected $model;

    /**
    +----------------------------------------------------------
     * 初始化
    +----------------------------------------------------------
     */
    public function _initialize() {
        parent::_initialize();
        $this->model = M('Banner');
    }

    public function index(){
        $where=[];
        $search=I('post.search',[]);
        $pageSize = I('post.pageSize',20,'intval');
        $pageCurrent = I('post.pageCurrent',1,'intval');
        $pageCurrent = max(1,$pageCurrent);
        if(!empty($search['atime'])){
            $where['atime']=['between',[strtotime($search['atime']),strtotime($search['atime'].' 23:59:59')]];
        }
        $where['type']=0;
        $count= $this->model->where($where)->count();
        $list= $this->model->where($where)->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();
        $this->assign('search',$search);
        $this->assign('total',$count);
        $this->assign('list',$list);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $data['title'] = I('post.title','','trim');
            $data['href'] = I('post.href','','trim');
            $data['imgs'] = I('post.imgs','','trim');
            if(empty($data['title']) || empty($data['href']) || empty($data['imgs'])){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
            }
            $data['atime']=time();
            $data['type']=0;

            //@author-tc
            $only_title = $this->model->where("title = '{$data['title']}'")->find();
            if($only_title) {
                $this->ajaxReturn(['statusCode' => 300, 'message' => '标题已经存在！']);
            }
            //@author-tc

            $ref=$this->model->data($data)->add();
            if(empty($ref)){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '保存失败']);
            }else{
                $this->ajaxReturn(['statusCode' => 200, 'message' => 'banner设置成功']);
            }
        }else {
            $this->display();
        }
    }

    /**
     * 编辑广告F_dw
     */
    public function edit(){
        if(IS_POST){
            $data['title'] = I('post.title','','trim');
            $data['href'] = I('post.href','','trim');
            $data['imgs'] = I('post.imgs','','trim');
            if(empty($data['title']) || empty($data['href']) || empty($data['imgs'])){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
            }
            //$data['atime']=time();
            $data['type']=0;
            $bid = I('post.bid');
            //@author-tc
            $only_title = $this->model->where("title = '{$data['title']}'")->find();
            if($only_title['bid'] && $only_title['bid'] != $bid) {
                $this->ajaxReturn(['statusCode' => 300, 'message' => '标题已经存在！']);
            }
            //@author-tc

            $ref=$this->model->where("bid = '{$bid}'")->save($data);
            if(empty($ref)){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '保存失败']);
            }else{
                $this->ajaxReturn(['statusCode' => 200, 'message' => 'banner设置成功']);
            }
        }else {
            $bid = I('get.id');
            $data = $this->model->where("bid = '{$bid}'")->find();
            $this->assign('data',$data);
            $this->display();
        }
    }

    /**
     * 删除视频
     */
    public function del()
    {
        $id = I('get.bid','0','intval');
        $ids = I('get.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $del = $this->model->where(['bid'=>['in',$ids]])->delete();
        if($del){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '删除成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '删除失败']);
        }
    }

    public function upload(){
        $upload           = new \Think\Upload();
        $upload->exts     = array('jpg', 'gif', 'png', 'jpeg');
        $upload->hash     = true;
        $upload->rootPath = WEB_ROOT;
        $upload->savePath = 'Uploads' . DIRECTORY_SEPARATOR . 'banner' . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m') . DIRECTORY_SEPARATOR . date('d') . DIRECTORY_SEPARATOR;
        $upload->replace  = false;
        $upload->autoSub  = true;
        $info             = $upload->uploadOne($_FILES['file']);
        if (empty($info)) {
            $this->ajaxReturn(['statusCode' => 300, 'message' => $upload->getError()]);
        }
        $file = str_replace('\\','/',$info['savepath']).$info['savename'];
        $url = U($file, '', '', true);
        $this->ajaxReturn(['statusCode' => 200, 'message' => '上传成功','filename'=>$file]);
    }
}