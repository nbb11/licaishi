<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/10/26
 * Time: 20:12
 */

namespace Admin\Controller;


use Home\Controller\CommonController;

class TasksController extends CommonController
{
    public function index()
    {
        $where=[];
        $search=I('post.search',[]);
        $pageSize = I('post.pageSize',20,'intval');
        $pageCurrent = I('post.pageCurrent',1,'intval');
        $pageCurrent = max(1,$pageCurrent);
        if(!empty($search['atime'])){
            $where['atime']=['between',[strtotime($search['atime']),strtotime($search['atime'].' 23:59:59')]];
        }
        $db = M('Tasks');
        $where['type']=0;
        $count= $db->where($where)->count();
        $list= $db->where($where)->limit((($pageCurrent - 1) * $pageSize) . ',' . $pageSize)->select();
        $this->assign('search',$search);
        $this->assign('total',$count);
        $this->assign('list',$list);
        $this->display();
    }
    /* F_dw */
    public function edit(){
        if(IS_POST) {
            $data['groups'] = I('post.groups',0,'intval');
            $data['genre'] = I('post.genre',0,'intval');
            $data['integration'] = I('post.integration',1,'intval');
            $data['degree'] = I('post.degree',1,'intval');
            $data['period'] = I('post.period',0,'intval');
            $data['periodtype'] = I('post.periodtype',0,'intval');
            $data['desp'] = I('post.desp','','trim');
            $data['title'] = I('post.title','','trim');
            $data['adsimg'] = I('post.adsimg','','trim');
            $data['icon'] = I('post.icon','','trim');
            $data['id']  = I('post.id','','intval');

            $db = M('Tasks');
            $ref= $db->data($data)->save();
            if(is_bool($ref)){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '操作失败']);
            }else{
                $this->ajaxReturn(['statusCode' => 200, 'message' => '操作成功', 'closeCurrent' => true]);
            }
        } else {
            $db = M('Tasks');
            $id = I('get.id','','intval');
            $data = $db->where("id = '{$id}'")->find();
            $this->assign('data',$data);
            $this->display();
        }
    }

    public function add(){
        if(IS_POST){
            $data['adsimg'] = I('post.adsimg','','trim');
            $data['icon'] = I('post.icon','','trim');
            $data['groups'] = I('post.groups',0,'intval');
            $data['genre'] = I('post.genre',0,'intval');
            $data['integration'] = I('post.integration',1,'intval');
            $data['degree'] = I('post.degree',1,'intval');
            $data['period'] = I('post.period',0,'intval');
            $data['periodtype'] = I('post.periodtype',0,'intval');
            $data['desp'] = I('post.desp','','trim');
            $data['title'] = I('post.title','','trim');
            if(empty($data['adsimg']) || empty($data['icon']) || empty($data['desp']) || empty($data['title'])){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '参数错误']);
            }

            //@author-tc
            $db = M('Tasks');
            $only_title = $db->where("title = '{$data['title']}'")->find();
            if($only_title) {
                $this->ajaxReturn(['statusCode' => 300, 'message' => '任务名称已经存在！']);
            }
            //@author-tc

            $data['atime']=time();
            $data['status']=1;

            $ref= $db->data($data)->add();
            if(is_bool($ref)){
                $this->ajaxReturn(['statusCode' => 300, 'message' => '操作失败']);
            }else{
                $this->ajaxReturn(['statusCode' => 200, 'message' => '操作成功', 'closeCurrent' => true]);
            }
        }else{
            $this->display();
        }
    }

    public function del(){
        $id = I('get.bid','0','trim');
        $ids = I('get.ids','0','trim');
        $ids = empty($id)?$ids:$id;
        if(empty($ids)){
            $this->ajaxReturn(['statusCode' => 300, 'message' => '无效参数']);
        }
        $ids = explode(',',$ids);
        $dbrep=M('Tasks');
        $del = $dbrep->where(['id'=>['in',$ids]])->delete();
        if(!is_bool($del)){
            $this->ajaxReturn(['statusCode' => 200, 'message' => '处理成功']);
        }else{
            $this->ajaxReturn(['statusCode' => 300, 'message' => '处理失败']);
        }
    }

    public function upload(){
        $upload           = new \Think\Upload();
        $upload->exts     = array('jpg', 'gif', 'png', 'jpeg');
        $upload->hash     = true;
        $upload->rootPath = WEB_ROOT;
        $upload->savePath = 'Uploads' . DIRECTORY_SEPARATOR . 'tasks' . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m') . DIRECTORY_SEPARATOR . date('d') . DIRECTORY_SEPARATOR;
        $upload->replace  = false;
        $upload->autoSub  = true;
        $info             = $upload->uploadOne($_FILES['file']);
        if (empty($info)) {
            $this->ajaxReturn(['statusCode' => 300, 'message' => $upload->getError()]);
        }
        $file = str_replace('\\','/',$info['savepath']).$info['savename'];
        $url = U($file, '', '', true);
        $this->ajaxReturn(['statusCode' => 200, 'message' => '上传成功','filename'=>$file]);
    }
}