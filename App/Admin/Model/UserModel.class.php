<?php

/**
 * @author 小策一喋 <xvpindex@qq.com>
 * @link http://www.topstack.cn
 * @copyright Copyright (C) 2014 EWSD
 * @datetime 2014-10-15 12:40
 * @version 1.0
 * @description
 */

namespace Admin\Model;

use Think\Model;
use Common\Model\CommonModel;

class UserModel extends CommonModel {

    /**
      +----------------------------------------------------------
     * 定义
      +----------------------------------------------------------
     */
    public $model = 'User';

    public $tableFields = array(
      'uid' => array('name'=>'UID', 'order'=>'1'),
      'username' => array('name'=>'账号', 'order'=>'1'),
      'name' => array('name'=>'姓名', 'order'=>'1'),
      'roleid' => array('name'=>'角色', 'order'=>'1'),//F_dw
      'sex' => array('name'=>'性别', 'order'=>'0'),
      'status' => array('name'=>'状态', 'order'=>'1'),
    );

    //@author-tc
    protected $_validate = array(
      array('username', 'require', '用户名必填！'),
      array('name', 'require', '姓名必填！'),
      array('name', '', '姓名已存在！', 0, 'unique', self::MODEL_INSERT),
      array('pwd', 'require', '密码必填！',0,'',self::MODEL_INSERT),
      array('email', 'require', '邮箱必填！'),
      array('email', 'email', '邮箱格式错误！', 2),
      array('mobile', 'require', '手机必填！'),
      array('mobile','/^(13[0-9]|14[5|7]|15[0-9]|17[6|7|8]|18[0-9])\d{8}$/','手机格式错误',self::MUST_VALIDATE,'regex',3),
      array('tel','/^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/','电话格式错误',self::MUST_VALIDATE,'regex',3),
    );
    //@author-tc

    protected $_auto = array(
      array('pass', 'md5', 3, 'function'),
      array('ifadmin', '0', self::MODEL_INSERT),
      array('ip', 'get_client_ip', 3, 'function'),
      array('createtime', 'time', 3, 'function'),
    );

    /**
      +----------------------------------------------------------
     * 管理员列表
      +----------------------------------------------------------
     */
    public function index($condition) {

        $list = $this->getPageList($param = array('modelName' => 'User', 'field' => '*', 'order' => 'uid ASC', 'listRows' => '20'), $condition);
        foreach ($list['info'] as $k => $v) {
            //$list['info'][$k]['name'] = CommonModel::getUserFullNameByUid($v['uid']);
            $list['info'][$k]['sex'] = $v['sex'] == 1 ? '男' : '女';
            $list['info'][$k]['company'] = $v['UserOrg'][0]['company'];
            $list['info'][$k]['department'] = $v['UserOrg'][0]['department'];
            $list['info'][$k]['office'] = $v['UserOrg'][0]['office'];
            $list['info'][$k]['duty'] = $v['UserOrg'][0]['duty'];
            $list['info'][$k]['status'] = $v['status'] == 1 ? "启用" : "禁用";
            $list['info'][$k]['roleid'] = M('Role')->where("id = '{$v['roleid']}'")->getField('name');
        }
        return $list;
    }

    /**
      +----------------------------------------------------------
     * 数据详情
      +----------------------------------------------------------
     */
    public function detail($condition){
        return $this->getDetail($param = array('modelName' => $this->model), $condition);
    }

    //protected $trueTableName = 'top_user';
    //获得用户详情
    public function getMemberDetailByUid($uid) {

        if (isset($uid)) {
            $map["uid"] = $uid;
            return M("User")->where($map)->find();
        }
    }

    //获得用户列表
    public function getMemberListByAll() {

        return M("User")->select();
    }

    //编辑用户
    public function edit() {
        $M = M("User");
        $data = $_POST['info'];
        $data['update_time'] = time();
        if ($M->save($data)) {
            return array('status' => 1, 'info' => "已经更新", 'url' => U('Member/index'));
        } else {
            return array('status' => 0, 'info' => "更新失败，请刷新页面尝试操作");
        }
    }

    public function myInfo($datas) {
        $M = M("User");
        if (md5($datas['pwd0'] . $datas['username']) != $_SESSION['myInfo']['pwd']) {
            return array('statusCode' => 300, 'message' => "旧密码错误");
        }
        if (trim($datas['pwd']) == '') {
            return array('statusCode' => 300, 'message' => "密码不能为空");
        }
        if (trim($datas['pwd']) != trim($datas['pwd1'])) {
            return array('statusCode' => 300, 'message' => "两次密码不一致");
        }
        $data['uid'] = $_SESSION['myInfo']['uid'];
        $data['pwd'] = md5($datas['pwd'] . $datas['username']);
        if ($M->save($data)) {
            setcookie("$this->loginMarked", NULL, -3600, "/");
            unset($_SESSION["$this->loginMarked"], $_COOKIE["$this->loginMarked"]);
            return array('statusCode' => 200, 'message' => "你的密码已经成功修改，请重新登录", 'url' => U('Access/index'));
        } else {
            return array('statusCode' => 300, 'message' => "密码修改失败");
        }
    }

}

?>
