/**
 * Created by Administrator on 2015/10/26.
 */
(function(o) {
    Object.extend = function(a, b) {
        for (var c in b) {
            a[c] = b[c]
        }
        return a
    };
    Object.extend(Date.prototype, {
        Format: function(a) {
            var o = {
                "M+": this.getMonth() + 1,
                "d+": this.getDate(),
                "h+": this.getHours(),
                "m+": this.getMinutes(),
                "s+": this.getSeconds(),
                "q+": Math.floor((this.getMonth() + 3) / 3),
                "S": this.getMilliseconds()
            };
            if (/(y+)/.test(a)) a = a.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o) if (new RegExp("(" + k + ")").test(a)) a = a.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return a
        },
        addDays: function(d) {
            this.setDate(this.getDate() + d);
        },
        addWeeks: function(w) {
            this.addDays(w * 7);
        },
        addMonths: function(m) {
            var d = this.getDate();
            this.setMonth(this.getMonth() + m);
            if (this.getDate() < d) this.setDate(0)
        },
        addYears: function(y) {
            var m = this.getMonth();
            this.setFullYear(this.getFullYear() + y);
            if (m < this.getMonth()) {
                this.setDate(0);
            }
        },
        toDateString: function(c) {
            var d = this;
            var e = function() {};
            e.prototype = {
                year: function() {
                    if (c.indexOf("yyyy") >= 0) {
                        return d.getYear();
                    } else {
                        return d.getYear().toString().substr(2);
                    }
                },
                elseTime: function(a, b) {
                    return b >= 0 ? (a < 10 ? "0" + a: a) : (a);
                },
                month: function() {
                    return this.elseTime(d.getMonth() + 1, c.indexOf("MM"));
                },
                day: function() {
                    return this.elseTime(d.getDate(), c.indexOf("dd"));
                },
                hour: function() {
                    return this.elseTime(d.getHours(), c.indexOf("hh"));
                },
                minute: function() {
                    return this.elseTime(d.getMinutes(), c.indexOf("mm"));
                },
                second: function() {
                    return this.elseTime(d.getSeconds(), c.indexOf("ss"));
                }
            };
            var f = new e();
            var g = {
                year: ["yyyy", "yy"],
                month: ["MM", "M"],
                day: ["dd", "d"],
                hour: ["hh", "h"],
                minute: ["mm", "m"],
                second: ["ss", "s"]
            };
            for (var h in g) {
                c = c.replace(g[h][0], eval("tV." + h + "()"));
                c = c.replace(g[h][1], eval("tV." + h + "()"))
            }
            return c
        }
    });
    var r = (function(b, c) {
        this.element = b;
        this.handler = c;
        b.addEventListener('touchstart', this, false);
        b.addEventListener('click', this, false);
        r.prototype.handleEvent = function(a) {
            switch (a.type) {
                case 'touchstart':
                    this.onTouchStart(a);
                    break;
                case 'touchmove':
                    this.onTouchMove(a);
                    break;
                case 'touchend':
                    this.onClick(a);
                    break;
                case 'click':
                    this.onClick(a);
                    break
            }
        };
        r.prototype.onTouchStart = function(a) {
            a.stopPropagation();
            this.element.addEventListener('touchend', this, false);
            document.body.addEventListener('touchmove', this, false);
            this.startX = a.touches[0].clientX;
            this.startY = a.touches[0].clientY;
            isMoving = false
        };
        r.prototype.onTouchMove = function(a) {
            if (Math.abs(a.touches[0].clientX - this.startX) > 10 || Math.abs(a.touches[0].clientY - this.startY) > 10) {
                this.reset()
            }
        };
        r.prototype.onClick = function(a) {
            this.reset();
            this.handler.call(this.element, a);
            if (a.type == 'touchend') {
                preventGhostClick(this.startX, this.startY)
            }
        };
        r.prototype.reset = function() {
            this.element.removeEventListener('touchend', this, false);
            document.body.removeEventListener('touchmove', this, false)
        };
        function preventGhostClick(x, y) {
            d.push(x, y);
            window.setTimeout(gpop, 2500)
        }
        function gpop() {
            d.splice(0, 2)
        }
        function gonClick(a) {
            for (var i = 0; i < d.length; i += 2) {
                var x = d[i];
                var y = d[i + 1];
                if (Math.abs(a.clientX - x) < 25 && Math.abs(a.clientY - y) < 25) {
                    a.stopPropagation();
                    a.preventDefault()
                }
            }
        }
        document.addEventListener('click', gonClick, true);
        var d = []
    });
    $.fn.fastclick = function(a) {
        this.each(function() {
            new r(this,
                function() {
                    a.call(this)
                })
        });
        return this
    };
    $.fn.fire = function(a) {
        var e = document.createEvent('HTMLEvents');
        e.initEvent(a, false, false);
        console.log(this[0]);
        //this[0].dispatchEvent(e);
    };
    Function.prototype.Bind = function() {
        var a = this,
            object = arguments[0],
            args = new Array();
        for (var i = 1; i < arguments.length; i++) {
            args.push(arguments[i])
        }
        return function() {
            return a.apply(object, args)
        }
    };
    var u = {
        getDefaultOffset: function(a) {
            var b = document;
            var c = b.documentElement,
                body = b.body,
                defaultView = b.defaultView,
                computedStyle = defaultView ? defaultView.getComputedStyle(a, null) : a.currentStyle,
                patrn = /^t(?:able|h|d)/i,
                offsetParent = a.offsetParent,
                l = a.offsetLeft,
                t = a.offsetTop;
            while ((a = a.parentNode) && a !== body && a !== c) {
                computedStyle = defaultView ? defaultView.getComputedStyle(a, null) : a.currentStyle;
                l -= a.scrollLeft;
                t -= a.scrollTop;
                if (a === offsetParent) {
                    l += a.offsetLeft;
                    t += a.offsetTop;
                    offsetParent = a.offsetParent
                }
            }
            if (~'static,relative'.indexOf(computedStyle.position)) {
                l += body.offsetLeft;
                t += body.offsetTop
            }
            return {
                left: l,
                top: t
            }
        }
    };
    var z = function() {
        this.init.apply(this, arguments)
    };
    Object.extend(z.prototype, {
        init: function(c, w, h, a) {
            this.canvas = document.createElement('canvas');
            this.canvas.width = w;
            this.canvas.height = h;
            if (a) this.canvas.className = a;
            document.getElementById(c).appendChild(this.canvas);
            this.context = this.canvas.getContext('2d')
        },
        width: function() {
            return this.canvas.width
        },
        height: function() {
            return this.canvas.height
        },
        setCssText: function(a) {
            this.canvas.style.cssText = a
        },
        setContextStyle: function(a) {
            for (var i in a) {
                a.hasOwnProperty(i) && (this.context[i] = a[i])
            }
        },
        begin: function() {
            this.context.beginPath()
        },
        close: function() {
            this.context.closePath()
        },
        save: function() {
            this.context.save()
        },
        stroke: function() {
            this.context.stroke()
        },
        restore: function() {
            this.context.restore()
        },
        clear: function(a) {
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.canvas.width = this.canvas.width
        },
        remove: function() {
            this.clear();
            this.canvas.parentNode.removeChild(this.canvas)
        },
        scale: function(x, y) {
            this.context.scale(x || 1, y || 1)
        },
        drawLine: function(a, b, c) {
            c = c || {};
            var d = 0.5,
                yd = 0.5;
            c.style && this.setContextStyle(c.style);
            this.context.moveTo(a.x + d, a.y + yd);
            this.context.lineTo(b.x + d, b.y + yd)
        },
        drawStrokeLine: function(a, b, c) {
            c = c || {};
            var d = 0.5,
                yd = 0.5;
            this.context.beginPath();
            c.style && this.setContextStyle(c.style);
            this.context.moveTo(a.x + d, a.y + yd);
            this.context.lineTo(b.x + d, b.y + yd);
            this.context.closePath();
            this.context.stroke()
        },
        drawShapMask: function(a, b, c, d) {
            c = c || {};
            var e = 0.5,
                yd = 0.5;
            this.context.beginPath();
            c.style && this.setContextStyle(c.style);
            if (d) {
                var f = this.context.createLinearGradient(0, 0, 0, b);
                f.addColorStop(0, c.style.fillStyle);
                f.addColorStop(1, c.style.strokeStyle);
                this.context.fillStyle = f
            }
            if (a && a.p.length > 0) {
                this.context.moveTo(a.start.x + e, a.start.y + yd);
                for (var i in a.p) {
                    this.context.lineTo(a.p[i].x + e, a.p[i].y + yd)
                }
                this.context.lineTo(a.p[i].x + e, b + yd);
                this.context.lineTo(a.start.x + e, b + yd);
                this.context.lineTo(a.start.x + e, a.start.y + yd)
            }
            this.context.closePath();
            this.context.fill();
            this.context.stroke()
        },
        drawDashLine: function(a, b, c) {
            c = c || {};
            var d = c.dd || 2,
                gd = c.gd || 2,
                xd = 0.5,
                yd = 0.5,
                s = d + gd,
                xb = a.x + xd,
                xe = b.x + xd,
                yb = a.y + yd,
                ye = b.y + yd;
            c.style && this.setContextStyle(c.style);
            if (a.y === b.y) {
                for (var i = xb; i < xe; i += s) {
                    this.context.moveTo(i, yb);
                    this.context.lineTo(Math.min(i + d, xe), yb)
                }
            } else if (a.x === b.x) {
                for (var i = yb; i < ye; i += s) {
                    this.context.moveTo(xb, i);
                    this.context.lineTo(xb, Math.min(i + d, ye))
                }
            }
        },
        drawRect: function(a, b, c) {
            c = c || {};
            var d = 0.5,
                yd = 0.5;
            c.style && this.setContextStyle(c.style);
            this.context.strokeRect(a.x + d, a.y + yd, b.w, b.h)
        },
        drawFillRect: function(a, b, c) {
            c = c || {};
            c.style && this.setContextStyle(c.style);
            this.context.fillRect(a.x, a.y, b.w, b.h)
        }
    });
    var A = function() {
        this.init.apply(this, arguments)
    };
    o["dCode"] = thisCode;//"3000592";
    Object.extend(A.prototype, {
        colors: {
            'blue': '#5182cb',
            'gray': '#c2c2c2',
            'PRICE': '#0259a8',
            'MA': '#ffad11',
            'VOL0': '#989898',
            'dime': '#4489ca',
            'black': '#000000',
            'green': '#bf3130',
            'red': '#008001',
            'ma5': '#000000',
            'ma10': '#ffad11',
            'ma20': '#ef00ff'
        },
        JSData: {},
        data: {},
        QuoteInfo: {
            name: '未知',
            code: '000000'
        },
        canvasOpt: {
            container: null,
            wdmxContainer: null,
            width: 288,
            height: 160,
            currentType: 0,
            showTarget: true,
            levelNum: 3,
            verticalNum: 3,
            currentKS: 60,
            api: ''
        },
        init: function(a) {
            this.Container = document.getElementById('canvasContainer');
            this.WdmxContainer = document.getElementById(a.wdmxContainer);
            if (!this.Container) {
                alert('找不到指定容器');
                return
            }
            this.fixedSize = {
                w: a.width,
                h: a.height
            };
            this.canvasOpt = a;
            this.currentKS = {};
            this.kChart_Min = 20;
            this.kChart_Max = 80;
            this.Container.style.position = 'relative';
            this.currentType = 'TimePlan';
            this.initCanvas("初始化");
            this.bindDrawDimensionEvent();
            this.bindOrientationChangeEvent()
        },
        initCanvas: function(a) {
            /*if (this.currentType == "Timeplan") {
                this.WdmxContainer.className = "content_canvas more"
            } else {
                this.WdmxContainer.className = "content_canvas"
            }*/
            this.canvasOpt.width = this.fixedSize.w || this.Container.clientWidth;
            this.canvasOpt.height = this.fixedSize.h || this.Container.clientHeight;
            if (this.canvasOpt.width < 100 || this.canvasOpt.height < 80) {
                this.canvasOpt.width = 282
            }
            this.Container.innerHTML = '';
            this.k_ctrl = document.createElement("span");
            this.k_ctrl.className = "k_ctrl";
            this.k_minus = document.createElement("span");
            this.k_minus.className = "k_minus";
            this.k_ctrl.appendChild(this.k_minus);
            this.k_added = document.createElement("span");
            this.k_added.className = "k_added";
            this.k_ctrl.appendChild(this.k_added);
            this.Container.appendChild(this.k_ctrl);
            var b = this,
                _type = '';
            $(".k_ctrl span").fastclick(function() {
                _type = b.currentType;
                b.currentKS[_type] = b.currentKS[_type] || b.canvasOpt.currentKS;
                if (this.className == "k_minus") {
                    b.currentKS[_type] += 20
                } else if (this.className == "k_added") {
                    b.currentKS[_type] -= 20
                }
                b.renderK()
            });
            this.MATip = document.createElement("span");
            this.MATip.className = "maTips";
            this.Container.appendChild(this.MATip);
            this.coordinate = this.getCoordinate();
            this.chartBgCanvas = new z(this.canvasOpt.container, this.canvasOpt.width, this.coordinate.mH);
            this.chartCanvas = new z(this.canvasOpt.container, this.canvasOpt.width - (this.coordinate.xb * 2), this.coordinate.myH);
            this.dimensionCanvas = new z(this.canvasOpt.container, this.canvasOpt.width - (this.coordinate.xb * 2), this.coordinate.myH);
            this.chartBgCanvas.setCssText("position:absolute; top:0; left:0; z-index:1;");
            this.chartCanvas.setCssText('position:absolute; top:' + this.coordinate.yb + 'px; left:' + this.coordinate.xb + 'px; z-index:2;');
            this.dimensionCanvas.setCssText('position:absolute; top:' + this.coordinate.yb + 'px; left:' + this.coordinate.xb + 'px; z-index:2;');
            this.dimensionTip_y = document.createElement("p");
            this.dimensionTip_y.style.cssText = "position:absolute;top:0;left:0;width:100%;z-index:3;height:15px;";
            this.dimensionTip_x = document.createElement("cite");
            this.dimensionTip_x.style.cssText = "position:absolute;top:" + (this.coordinate.myH + 2) + "px;left:0;font:normal 12px/14px Arial;background-color:#4489ca;color:#fff; padding:0 2px;z-index:1;";
            this.Container.appendChild(this.dimensionTip_x);
            this.Container.appendChild(this.dimensionTip_y);
            this.drawMainCoordinate();
            if (this.canvasOpt.showTarget) {
                this.targetBgCanvas = new z(this.canvasOpt.container, this.canvasOpt.width, this.coordinate.tH);
                this.targetCanvas = new z(this.canvasOpt.container, this.canvasOpt.width - (this.coordinate.xb * 2), this.coordinate.tH - (this.coordinate.yb * 2));
                this.targetBgCanvas.setCssText("position:absolute; bottom:0; left:0;");
                this.targetCanvas.setCssText('position:absolute; bottom:' + this.coordinate.yb + 'px; left:' + this.coordinate.xb + 'px;');
                this.drawTargetCoordinate()
            }
        },
        getCoordinate: function() {
            var b = this.canvasOpt.width,
                cH = this.canvasOpt.height,
                tpH = 14,
                tH = this.canvasOpt.showTarget ? (5 * cH / 16) - (tpH / 2) : 0,
                mH = this.canvasOpt.showTarget ? (cH - tpH - tH) : (cH - tpH),
                lUnit = mH / (this.canvasOpt.levelNum + 1);
            vUnit = b / (this.canvasOpt.verticalNum + 1);
            xb = 1,
                yb = 1,
                lw = 1,
                xm = Math.round(b / 2),
                a = 0;
            return {
                cW: b,
                cH: cH,
                tpH: tpH,
                lUnit: lUnit,
                vUnit: vUnit,
                tH: tH,
                mH: mH,
                xb: xb,
                yb: yb,
                lw: lw,
                xe: b,
                ye: mH,
                xm: xm,
                myH: mH - (yb * 2),
                myW: b - (lw * 2),
                tyH: tH - (yb * 2)
            }
        },
        drawMainCoordinate: function() {
            var a = this.chartBgCanvas,
                _cOpt = this.coordinate;
            a.clear();
            a.setContextStyle({
                strokeStyle: this.colors['blue']
            });
            this.levelTip = [];
            for (var i = 0; i < this.canvasOpt.levelNum + 2; i++) {
                this.levelTip.push(document.createElement("p"));
                this.Container.appendChild(this.levelTip[i])
            }
            this.verticalTip = [];
            this.verticalTip_SetX = {};
            for (var i = 0; i < this.canvasOpt.verticalNum + 2; i++) {
                this.verticalTip.push(document.createElement("pre"));
                this.Container.appendChild(this.verticalTip[i])
            }
            var b = {
                x: {
                    x: 0,
                    y: 0
                },
                y: {
                    w: parseInt(_cOpt.cW - _cOpt.lw),
                    h: parseInt(_cOpt.mH - _cOpt.lw)
                }
            };
            a.drawRect(b.x, b.y);
            a.setContextStyle({
                strokeStyle: this.colors['gray']
            });
            a.begin();
            var c = {
                x: {
                    x: _cOpt.xb,
                    y: Math.round(_cOpt.mH / 2)
                },
                y: {
                    x: _cOpt.xe - 2,
                    y: Math.round(_cOpt.mH / 2)
                }
            };
            a.drawLine(c.x, c.y);
            if (this.canvasOpt.currentType == 0) {
                var d = {
                    x: {
                        x: _cOpt.xm,
                        y: _cOpt.yb
                    },
                    y: {
                        x: _cOpt.xm,
                        y: _cOpt.ye - 2
                    }
                };
                a.drawLine(d.x, d.y)
            }
            var e, _y;
            for (var i = 1; i <= this.canvasOpt.levelNum; i++) {
                _y = Math.round(i * _cOpt.lUnit);
                a.drawDashLine({
                        x: _cOpt.xb,
                        y: _y
                    },
                    {
                        x: _cOpt.xe - 2,
                        y: _y
                    });
                this.levelTip[i].style.cssText = "top:" + (_y - (this.levelTip[i].clientHeight / 2)) + "px;"
            }
            this.levelTip[i].style.cssText = "top:" + (_cOpt.mH - this.levelTip[i].clientHeight) + "px;";
            var f = '';
            for (var i = 0; i <= this.canvasOpt.verticalNum; i++) {
                f = '';
                e = Math.round(i * _cOpt.vUnit);
                if (i > 0) {
                    a.drawDashLine({
                            x: e,
                            y: _cOpt.yb
                        },
                        {
                            x: e,
                            y: _cOpt.ye - 2
                        });
                    f = "text-align:center;left:" + (e - (this.verticalTip[i].clientWidth / 2)) + "px;"
                }
                if (this.canvasOpt.showTarget) {
                    f += "margin-bottom:" + this.coordinate.tH + "px"
                }
                this.verticalTip[i].style.cssText = f;
                this.verticalTip_SetX["_x_" + e] = true
            }
            f = "text-align:right;left:" + (_cOpt.cW - (this.verticalTip[i].clientWidth)) + "px;";
            if (this.canvasOpt.showTarget) {
                f += "margin-bottom:" + this.coordinate.tH + "px"
            }
            this.verticalTip[i].style.cssText = f;
            a.close();
            a.stroke()
        },
        drawTargetCoordinate: function() {
            var a = this.targetBgCanvas,
                _cOpt = this.coordinate;
            a.clear();
            a.setContextStyle({
                strokeStyle: this.colors['blue']
            });
            var b = {
                x: {
                    x: 0,
                    y: 0
                },
                y: {
                    w: parseInt(_cOpt.cW - _cOpt.lw),
                    h: parseInt(_cOpt.tH - _cOpt.lw)
                }
            };
            a.drawRect(b.x, b.y);
            a.setContextStyle({
                strokeStyle: this.colors['gray']
            });
            a.begin();
            var c = {
                x: {
                    x: _cOpt.xb,
                    y: Math.round(_cOpt.tH / 2)
                },
                y: {
                    x: _cOpt.xe - 2,
                    y: Math.round(_cOpt.tH / 2)
                }
            };
            a.drawLine(c.x, c.y);
            if (this.canvasOpt.currentType == 0) {
                var d = {
                    x: {
                        x: _cOpt.xm,
                        y: _cOpt.yb
                    },
                    y: {
                        x: _cOpt.xm,
                        y: _cOpt.ye - 2
                    }
                };
                a.drawLine(d.x, d.y)
            }
            for (var i = 1; i <= this.canvasOpt.verticalNum; i++) {
                _x = Math.round(i * _cOpt.vUnit);
                a.drawDashLine({
                        x: _x,
                        y: _cOpt.yb
                    },
                    {
                        x: _x,
                        y: _cOpt.ye - 2
                    })
            }
            a.close();
            a.stroke()
        },
        drawDimension: function(x) {
            var p = this.data[this.currentType];
            if (!p || p.length <= 0) return false;
            var a = this.coordinate.xb,
                xe = this.coordinate.xe,
                s, xi, ti, v, cx, cy, fh = this.coordinate.tpH / 2,
                xw, yh, x1, y1;
            if (this.currentType !== 'Timeplan') {
                s = this.getCurrentTS(p.length)
            } else {
                s = this.getCurrentTS(242)
            }
            xi = Math.min(Math.ceil(x / s) || 1, p.length);
            ti = xi - 1;
            _item = p[ti];
            cx = _item.x;
            cy = _item.y;
            var b = this.dimensionCanvas;
            b.clear();
            b.setContextStyle({
                strokeStyle: this.colors['dime'],
                lineWidth: 1
            });
            b.begin();
            b.drawLine({
                    x: a,
                    y: cy
                },
                {
                    x: xe,
                    y: cy
                });
            b.drawLine({
                    x: cx,
                    y: 0
                },
                {
                    x: cx,
                    y: this.coordinate.ye
                });
            b.close();
            b.stroke();
            var c = this.currentType == "Timeplan" ? new Date(_item.d[0]) : new Date(_item.d[0].substr(0, 4) + "/" + _item.d[0].substr(4, 2) + "/" + _item.d[0].substr(6, 2));
            this.dimensionTip_x.innerHTML = this.currentType == "Timeplan" ? c.Format("hh:mm") : c.Format("MM月dd日");
            xw = this.dimensionTip_x.clientWidth;
            x1 = cx - (xw / 2);
            if (x1 < 0) {
                x1 = 0
            } else if (x1 + xw > xe) {
                x1 = xe - xw
            }
            this.dimensionTip_x.style.left = x1 + "px";
            this.dimensionTip_y.innerHTML = '';
            var d = "font:normal 12px/15px arial;background-color:#4489ca;color:#fff; padding:0 2px;";
            if (this.currentType !== 'Timeplan') {
                this.dimensionTip_y.innerHTML = '<cite style="' + d + '">' + Number(_item.d[1]).toFixed(2) + '</cite>';
                this.MATip.innerHTML = '<cite class="ma5">MA5:' + _item.info.ma5 + '</cite> <cite class="ma10">MA10:' + _item.info.ma10 + '</cite> <cite class="ma20">MA20:' + _item.info.ma20 + '</cite>'
            } else {
                var e = Number(_item.d[1] - _item.info.yc);
                var f = e.toFixed(2);
                var g = Number(e / _item.info.yc * 100).toFixed(2);
                this.dimensionTip_y.innerHTML = '<cite style="' + d + 'float:left;">' + Number(_item.d[1]).toFixed(2) + '</cite><cite style="' + d + 'float:right;">' + g + '%</cite>'
            }
            yh = this.dimensionTip_y.clientHeight;
            y1 = cy - Math.floor(yh / 2) + 1;
            if (y1 < 0) {
                y1 = 0
            } else if (y1 + yh > this.coordinate.myH) {
                y1 = this.coordinate.myH - yh + 2
            }
            this.dimensionTip_y.style.top = y1 + "px"
        },
        bindDrawDimensionEvent: function() {
            var b = this,
                chart = b.Container,
                canvasX = u.getDefaultOffset(chart).left,
                isDrawingOk = false,
                x = 0,
                y = 0,
                st,
                mt,
                et;
            chart.ontouchstart = function(e) {
                isDrawingOk = true;
                clearTimeout(st);
                clearTimeout(mt);
                clearTimeout(et);
                var a = e.touches[0];
                x = a.pageX;
                y = a.pageY;
                st = setTimeout(function() {
                        b.drawDimension(x - canvasX)
                    },
                    0)
            };
            chart.ontouchmove = function(e) {
                clearTimeout(st);
                clearTimeout(mt);
                clearTimeout(et);
                var a = e.touches[0],
                    xi = a.pageX,
                    yi = a.pageY;
                mt = setTimeout(function() {
                        isDrawingOk && b.drawDimension(xi - canvasX)
                    },
                    0);
                if (Math.abs(xi - x) > Math.abs(yi - y)) {
                    e.preventDefault();
                    return false
                }
            };
            chart.ontouchend = function(e) {
                isDrawingOk = false;
                clearTimeout(st);
                clearTimeout(mt);
                clearTimeout(et);
                et = setTimeout(function() {
                        b.dimensionCanvas.clear();
                        b.dimensionTip_x.innerHTML = " ";
                        b.dimensionTip_y.innerHTML = " ";
                        b.MATip.innerHTML = "";
                        b.updateLastInfo()
                    },
                    5)
            }
        },
        bindOrientationChangeEvent: function() {
            var b = this,
                chart = b.Container,
                ct, ctt, etype = o.onorientationchange ? 'onorientationchange': 'onresize',
                eHandler = function() {
                    clearTimeout(ct);
                    ct = setTimeout(function() {
                            var a = b.chartBgCanvas.width();
                            if (a && a !== b.Container.clientWidth) {
                                clearTimeout(ct);
                                b.initCanvas("旋转");
                                b.render("旋转重绘")
                            } else {
                                eHandler()
                            }
                        },
                        200)
                },
                clearHandler = function() {
                    ctt = setTimeout(function() {
                            clearTimeout(ct);
                            clearTimeout(ctt)
                        },
                        1000)
                };
            o[etype] = function() {
                eHandler();
                clearHandler()
            }
        },
        jsonp: function(b) {
            var s = document.createElement('script'),
                t = +new Date(),
                cb = 'em_data_' + this.currentType + '_' + t,
                abortTimeout,
                clearJsonp = function() {
                    s.parentNode.removeChild(s);
                    o[cb] && (delete o[cb])
                };
            s.onerror = function() {
                clearTimeout(abortTimeout);
                clearJsonp();
                b.error && b.error()
            };
            o[cb] = function(a) {
                clearTimeout(abortTimeout);
                clearJsonp();
                b.success(a)
            };
            b.timeout && (abortTimeout = setTimeout(function() {
                    clearJsonp();
                    b.error && b.error()
                },
                b.timeout));
            s.src = [b.url, '&jsname=', cb].join('');
            document.getElementsByTagName("head").item(0).appendChild(s)
        },
        getData: function(b) {
            refresh.start('Refresh Chart');
            var c = new Date();
            var d = this,
                wd = this.canvasOpt.Code || o["dCode"],
                url = '/Home/stock/data?id=' + wd;
            switch (this.currentType) {
                case 'Timeplan':
                    url += '&type=R';
                    break;
                case 'DailyK':
                    url += '&type=HD';
                    break;
                case 'WeekK':
                    url += '&type=HW';
                    break;
                case 'MonthK':
                    url += '&type=HM';
                    break;
                case 'Minute5':
                    url += '&type=HM5';
                    break;
                case 'Minute15':
                    url += '&type=HM15';
                    break;
                case 'Minute60':
                    url += '&type=HM60';
                    break
            }
            this.jsonp({
                url: url,
                success: function(a) {
                    refresh.end('Chart Loaded!');
                    if (a) {
                        d.JSData[d.currentType] = a;
                        d.render("数据加载完成")
                    } else {}
                },
                error: function() {
                    refresh.timeout()
                },
                timeout: 10000
            });
            if (this.hqTimeout) o.clearTimeout(this.hqTimeout);
            this.hqTimeout = o.setTimeout(this.getData.Bind(this), 60000)
        },
        setCurrent: function(a) {
            this.currentType = a;
            this.initCanvas("标签切换");
            this.getData()
        },
        render: function(a) {
            if (this.currentType) {
                return {
                    Timeplan: this.renderTimeplan.Bind(this),
                    DailyK: this.renderK.Bind(this),
                    WeekK: this.renderK.Bind(this),
                    MonthK: this.renderK.Bind(this),
                    Minute5: this.renderK.Bind(this),
                    Minute15: this.renderK.Bind(this),
                    Minute60: this.renderK.Bind(this)
                } [this.currentType]()
            }
        },
        updateLastInfo: function() {
            var a = this.data[this.currentType];
            if (a) {
                var b = a[a.length - 1];
                if (!b) {
                    b = {
                        d: [new Date(), 0, 0],
                        info: {
                            c: 0,
                            h: 0,
                            l: 0,
                            o: 0,
                            yc: 0,
                            ma5: 0,
                            ma10: 0,
                            ma20: 0
                        }
                    }
                }
                if (this.currentType != "Timeplan") {
                    this.MATip.innerHTML = '<cite class="ma5">MA5:' + b.info.ma5 + '</cite> <cite class="ma10">MA10:' + b.info.ma10 + '</cite> <cite class="ma20">MA20:' + b.info.ma20 + '</cite>'
                }
            }
        },
        renderTimeplan: function() {
            this.drawTChart()
        },
        renderK: function() {
            this.drawKChart()
        },
        getCurrentTS: function(a) {
            var b = 242;
            if (a) b = a;
            b -= 1;
            var c = this.coordinate.myW - 1,
                _cUnit = c / b;
            return _cUnit
        },
        getCurrentKS: function(a) {
            _t = a;
            var b = this.coordinate.myW,
                _cUnit = b / _t;
            return _cUnit
        },
        each: function(a, b) {
            var i, key;
            if (typeof a.length == 'number') {
                for (i = 0; i < a.length; i++) if (b.call(a[i], i, a[i]) === false) return a
            } else {
                for (key in a) if (b.call(a[key], key, a[key]) === false) return a
            }
            return a
        },
        getMaxMin: function(b, c, d) {
            var e = {
                max: [],
                min: []
            };
            this.each(b,
                function(i, v) {
                    var a = v.split(',');
                    e.max.push(a[c]);
                    e.min.push(a[d || c])
                });
            var f = Math.max.apply(null, e.max);
            var g = Math.min.apply(null, e.min);
            e = null;
            return {
                max: f,
                min: g,
                mid: f - (f - g) / 2
            }
        },
        getMaxMinN: function(c, d, e, f, g) {
            var h = {
                max0: [],
                min0: [],
                max1: [],
                min1: [],
                maxV: [],
                minV: []
            };
            this.each(c,
                function(i, v) {
                    var a = v.split(',');
                    var b = Number(a[0].replace(':', ''));
                    if (g && !(b >= 930 && b <= 1500)) {
                        return
                    }
                    h.max0.push(a[d]);
                    h.min0.push(a[d]);
                    h.max1.push(a[e]);
                    h.min1.push(a[e]);
                    h.maxV.push(a[f]);
                    h.minV.push(a[f])
                });
            if (h.max0.length == 0) {
                return {
                    max: 0,
                    min: 0,
                    mid: 0,
                    maxV: 0,
                    minV: 0
                }
            } else {
                var j = Math.max.apply(null, h.max0);
                var k = Math.min.apply(null, h.min0);
                var l = Math.max.apply(null, h.max1);
                var m = Math.min.apply(null, h.min1);
                var n = Math.max.apply(null, h.maxV);
                var o = Math.min.apply(null, h.minV);
                var p = Math.max(j, l);
                var q = Math.min(k, m);
                h = null;
                return {
                    max: p,
                    min: q,
                    mid: p - (p - q) / 2,
                    maxV: n,
                    minV: o
                }
            }
        },
        getADColor: function(a, b) {
            var c = b - a;
            return (c > 0 && this.colors['green']) || (c < 0 && this.colors['red']) || this.colors['black']
        },
        drawKChart: function() {
            var c = new Date().getTime();
            var d = this,
                _cOpt = d.coordinate,
                _type = d.currentType,
                _data = d.JSData[_type],
                _dataLen = d.kChart_Max = _data.length,
                _myCurrentKs = d.currentKS[_type] || d.canvasOpt.currentKS,
                _myCurrentKs = _dataLen < _myCurrentKs ? _dataLen: _myCurrentKs,
                _showCount = Math.min(_myCurrentKs, _dataLen),
                _startIdx = _dataLen - _showCount,
                _showData = _data.concat(),
                _showData = _showData.splice(_startIdx, _dataLen),
                _ref = d.getMaxMinN(_showData, 3, 4, 6),
                ye = _cOpt.tH,
                ym = _cOpt.myH / 2,
                ts = d.getCurrentKS(_myCurrentKs),
                _rectWidth = Math.floor(ts * 0.7),
                _rectWidth = _rectWidth % 2 == 0 ? _rectWidth + 1 : _rectWidth,
                _rectSpacer = Math.floor((ts - _rectWidth) / 2),
                _rectStore = Math.floor(_rectWidth / 2),
                s11 = ts + 1,
                pl = _dataLen - 2 * ts,
                pt = _cOpt.pt = (_ref.max - _ref.mid) / (ym - _cOpt.yb),
                xi1 = 0,
                xi1t,
                xi2 = xi1,
                yi1 = ym,
                yi2 = '',
                oy = '',
                cy = '',
                _color = '',
                _maColor = '',
                maY1 = {},
                maY2 = {},
                kvX,
                kvY;
            $(".k_ctrl").show();
            if (_myCurrentKs >= d.kChart_Max) $(".k_minus").hide();
            else $(".k_minus").show();
            if (_myCurrentKs <= d.kChart_Min) $(".k_added").hide();
            else $(".k_added").show();
            d.chartCanvas.clear("kChart");
            if (this.canvasOpt.showTarget) {
                d.targetCanvas.clear("kChart_VOL")
            }
            d.data[_type] = [];
            var e = [];
            var f = Math.ceil(_showData.length / (d.levelTip.length - 1));
            var g = [5, 10, 20],
                _norm = '';
            var h = d.getMA(g, _data);
            d.each(_showData,
                function(i, v) {
                    _item = v.split(',');
                    _color = d.getADColor(_item[1], _item[2]);
                    if (i > 0) {
                        xi2 += ts
                    }
                    xi2t = Math.floor(xi2) + _rectStore + _rectSpacer;
                    xi1t = Math.floor(xi1) + _rectStore + _rectSpacer;
                    cy = ym - Math.round((_item[2] - _ref.mid) / pt);
                    oy = ym - Math.round((_item[1] - _ref.mid) / pt);
                    for (var a = 0; a < g.length; a++) {
                        _norm = "MA" + g[a];
                        if (h[_norm]) {
                            _maColor = d.colors[_norm.toLowerCase()];
                            maY2[_norm] = ym - Math.round((h[_norm][i + _startIdx] - _ref.mid) / pt);
                            if (i > 0) {
                                d.chartCanvas.drawStrokeLine({
                                        x: xi1t,
                                        y: maY1[_norm]
                                    },
                                    {
                                        x: xi2t,
                                        y: maY2[_norm]
                                    },
                                    {
                                        style: {
                                            lineWidth: 1,
                                            strokeStyle: _maColor
                                        }
                                    })
                            } else {
                                d.chartCanvas.drawStrokeLine({
                                        x: xi2t,
                                        y: maY2[_norm]
                                    },
                                    {
                                        x: xi2t,
                                        y: maY2[_norm]
                                    },
                                    {
                                        style: {
                                            lineWidth: 1,
                                            strokeStyle: _maColor
                                        }
                                    })
                            }
                            maY1[_norm] = maY2[_norm]
                        }
                    }
                    var b = Math.round(Math.abs(cy - oy));
                    b = b <= 0 ? 1 : b;
                    kvX = Math.floor(xi2) + _rectSpacer;
                    kvY = ye - ye * ((_ref.maxV - _item[6]) / _ref.maxV);
                    if (kvY < 5) kvY = 5;
                    d.chartCanvas.drawFillRect({
                            x: kvX,
                            y: ym - Math.round((Math.max(_item[1], _item[2]) - _ref.mid) / pt)
                        },
                        {
                            w: _rectWidth,
                            h: b
                        },
                        {
                            style: {
                                fillStyle: _color
                            }
                        });
                    if (d.canvasOpt.showTarget) {
                        d.targetCanvas.drawFillRect({
                                x: kvX,
                                y: ye
                            },
                            {
                                w: _rectWidth,
                                h: -kvY
                            },
                            {
                                style: {
                                    fillStyle: _color
                                }
                            })
                    }
                    d.chartCanvas.drawStrokeLine({
                            x: xi2t,
                            y: ym - Math.round((_item[3] - _ref.mid) / pt)
                        },
                        {
                            x: xi2t,
                            y: ym - Math.round((_item[4] - _ref.mid) / pt)
                        },
                        {
                            style: {
                                lineWidth: 1,
                                strokeStyle: _color
                            }
                        });
                    d.data[d.currentType].push({
                        d: _item,
                        info: {
                            ma5: h["MA5"][i],
                            ma10: h["MA10"][i],
                            ma20: h["MA20"][i]
                        },
                        x: xi2t,
                        y: cy
                    });
                    if (i == 0 || (i + 1) % f == 0 || i == _showCount - 1) {
                        e.push(_item[0].substr(4, 2) + "-" + _item[0].substr(6, 2))
                    }
                    xi1 = xi2
                });
            var j = (_ref.max - _ref.min) / (d.levelTip.length - 1);
            d.each(d.levelTip,
                function(i, a) {
                    a.innerHTML = '';
                    var b = document.createElement("span");
                    b.innerHTML = Number(_ref.max - i * j).toFixed(2);
                    a.appendChild(b)
                });
            d.each(d.verticalTip,
                function(i, a) {
                    a.innerHTML = e[i] || ''
                });
            if (_data && _data.length > 0) d.updateLastInfo()
        },
        drawTChart: function() {
            var e = this,
                _cOpt = e.coordinate,
                _data = e.JSData[e.currentType],
                _showData = _data.data,
                _ref = e.getMaxMinN(_showData, 1, 2, 3, true),
                ye = _cOpt.tH,
                ym = _cOpt.myH / 2,
                tc = e.colors['PRICE'],
                tj = e.colors['MA'],
                vlc = e.colors['VOL0'],
                ts = e.getCurrentTS(242),
                s11 = ts + 1,
                pl = _data.length - 2 * ts,
                vs = _cOpt.mH + _cOpt.tpH + 1,
                ve = _cOpt.cH,
                pt = _cOpt.pt = (_ref.max - _ref.mid) / (ym - _cOpt.yb),
                xi1 = 0,
                xi2 = xi1,
                yi1 = ym,
                yi2,
                xj1 = 0,
                xj2 = xj1,
                yj1 = ym,
                yj2,
                v_xi,
                v_yi;
            _ref.mid = _data.info.yc;
            if (!_ref.max) {
                _refUnitMx = 0
            } else if (!_ref.min) {
                _refUnitMx = _ref.mid * 0.001
            } else {
                _refUnitMx = Math.max(Math.abs(_ref.max) - _ref.mid, _ref.mid - Math.abs(_ref.min))
            }
            _ref.max = _ref.mid + _refUnitMx;
            _ref.min = _ref.mid - _refUnitMx;
            pt = _cOpt.pt = (_ref.max - _ref.mid) / (ym - _cOpt.yb);
            e.QuoteInfo = {
                name: _data.name,
                code: _data.code
            };
            e.chartCanvas.clear("tChart");
            if (this.canvasOpt.showTarget) {
                e.targetCanvas.clear("tChart_VOL")
            }
            e.data[e.currentType] = [];
            var f = [];
            var g = "00",
                _timeAt = '',
                _timeAtHour = 0;
            var h = new Date(_data.info.time.replace(/-/ig, "/")),
                _lastHour = h.getHours();
            var j = new Date(_data.info.time.replace(/-/ig, "/"));
            j.addDays( - 1);
            var k = {
                start: {},
                p: []
            };
            var l = [];
            e.each(_showData,
                function(i, v) {
                    _item = v.split(',');
                    var a = Number(_item[0].replace(':', ''));
                    if (a >= 930 && a <= 1500) {
                        l.push(v)
                    } else {}
                });
            e.each(l,
                function(i, v) {
                    _item = v.split(',');
                    var a = Number(_item[0].replace(':', ''));
                    if (a >= 930 && a <= 1500) {
                        _item.push((_item[1] - _data.info.yc) / _data.info.yc * 100);
                        yi2 = ym - Math.round((_item[1] - _ref.mid) / pt);
                        yj2 = ym - Math.round((_item[2] - _ref.mid) / pt);
                        v_yi = ye * ((_ref.maxV - _item[3]) / _ref.maxV);
                        if (ye - v_yi < 3) v_yi -= 3;
                        if (i == 0) {
                            yi1 = yi2;
                            yj1 = yj2
                        }
                        if (i > 0) {
                            if (i % ts === 0 && i < pl) {
                                xi2 += s11;
                                xj2 += s11
                            } else {
                                xi2 += ts;
                                xj2 += ts
                            }
                        }
                        e.chartCanvas.drawStrokeLine({
                                x: xi1,
                                y: yi1
                            },
                            {
                                x: xi2,
                                y: yi2
                            },
                            {
                                style: {
                                    lineWidth: 1,
                                    strokeStyle: tc
                                }
                            });
                        e.chartCanvas.drawStrokeLine({
                                x: xj1,
                                y: yj1
                            },
                            {
                                x: xj2,
                                y: yj2
                            },
                            {
                                style: {
                                    lineWidth: 1,
                                    strokeStyle: tj
                                }
                            });
                        if (e.canvasOpt.showTarget) {
                            v_xi = Math.round(xi1 + ((xi2 - xi1) / 2));
                            e.targetCanvas.drawStrokeLine({
                                    x: v_xi,
                                    y: ye
                                },
                                {
                                    x: v_xi,
                                    y: v_yi
                                },
                                {
                                    style: {
                                        lineWidth: 1,
                                        strokeStyle: vlc
                                    }
                                })
                        }
                        if (i == 0) k.start = {
                            x: xi1,
                            y: yi1
                        };
                        k.p.push({
                            x: xi2,
                            y: yi2
                        });
                        xi1 = xi2;
                        xj1 = xj2;
                        yi1 = yi2;
                        yj1 = yj2;
                        _timeAt = _item[0];
                        _timeAtHour = Number(_timeAt.substr(0, 2));
                        if (_lastHour < _timeAtHour) {
                            _item[0] = new Date(j.setHours(_timeAtHour)).setMinutes(_timeAt.substr(3, 2))
                        } else {
                            _item[0] = new Date(h.setHours(_timeAtHour)).setMinutes(_timeAt.substr(3, 2))
                        }
                        e.data[e.currentType].push({
                            d: _item,
                            x: xi2,
                            y: yi2,
                            info: _data.info
                        })
                    }
                });
            var m = (_ref.max - _ref.min) / (e.levelTip.length - 1);
            var n = Math.floor(e.levelTip.length / 2);
            var m = _refUnitMx / n;
            e.each(e.levelTip,
                function(i, a) {
                    a.innerHTML = '';
                    var b = Number(_ref.mid + (n - i) * m);
                    var c = document.createElement("span");
                    c.style.cssText = "float:left;" + (i < n ? "color:#c00;": (i == n) ? "": "color:#090;");
                    c.innerHTML = b.toFixed(2);
                    a.appendChild(c);
                    var d = document.createElement("span");
                    d.style.cssText = "float:right;" + (i < n ? "color:#c00;": (i == n) ? "": "color:#090;");
                    d.innerHTML = Number((b - _ref.mid) / _ref.mid * 100).toFixed(2) + "%";
                    a.appendChild(d)
                });
            var f = ['09:30', '10:30', '11:30/13:00', '14:00', '15:00'];
            e.each(e.verticalTip,
                function(i, a) {
                    a.innerHTML = f[i]
                });
            if (_showData && _showData.length > 0) e.updateLastInfo()
        },
        getMA: function(a, b) {
            var c = {},
                normsLen = a.length,
                norm = 0;
            for (var i = 0; i < normsLen; i++) {
                norm = a[i];
                c["MA" + norm] = []
            }
            var d = [],
                _item = [];
            var e = 0,
                _j = 0;
            while (e < b.length) {
                for (var i = 0; i < normsLen; i++) {
                    norm = a[i];
                    c["MA" + norm][e] = 0;
                    _j = 0;
                    while (_j < norm && _j < e + 1) {
                        _item = b[e - _j].split(',');
                        c["MA" + norm][e] += Number(_item[5]);
                        _j++
                    }
                    c["MA" + norm][e] = Number(c["MA" + norm][e] / _j).toFixed(2)
                }
                e++
            }
            return c
        },
        decimal: function(a, b) {
            b = Math.pow(10, b);
            return Math.round(a * b) / b
        }
    });
    function initChartTab() {
        if (!o.chartInit) {
            var b = new A({
                container: 'canvasContainer',
                wdmxContainer: 'wdmxContainer',
                currentType: 0,
                showTarget: true,
                levelNum: 3,
                verticalNum: 3,
                currentKS: 20,
                Code: o["scode"] ? o["scode"] : o["dCode"]
            });
            o.myChart = b;
            var c = $('#pic_plans_box').find('a');
            o.myChart.setCurrent("Timeplan");
            var d = "hover";
            c.fastclick(function() {
            	
                //显示图表
                var a = $(this),
                    _chart_tab = a.attr("tab");
                if(_chart_tab == "Timeplan"){
                	$("#wudang").show();
                	$("#canvasP").attr("class", "am-u-sm-8");
                }else{
                	$("#wudang").hide();
                	$("#canvasP").attr("class", "am-u-sm-12");
                }
                if (a.hasClass(d) && o.chartInit) return;
                c.removeClass(d);
                a.addClass(d);
                o["data"] = _chart_tab;
                o.myChart.setCurrent(o["data"]);
                if (o["data"] == "chart") b.setCurrent(_chart_tab);
              //隐藏面板
                $("#chartPanel>.am-in").attr("class", "am-tab-panel");
                $("#chartPanel .am-tabs-bd>.am-tab-panel:first").attr("class", "am-tab-panel am-active am-in");
                $('#pic_plans_box a').each(function(){
                	if($(this).attr("tab") == _chart_tab)
                		$(this).parent().attr("class", "am-active");
                	else
                		$(this).parent().attr("class", "");
                });
            });
            o.chartInit = true
        }
    }
    function setChartType(a) {}
    function refreshChart() {
        if (!o.chartInit) {
            initChartTab();
            var a = $('#pic_plans_box').find('span')[0];
            $(a).fire("click")
        } else {
            o.myChart.setCurrent(o["data"]);
            if (o["data"] == "chart") _myChart.setCurrent(_chart_tab)
        }
    }
    o.refreshChart = refreshChart
})(window); (function() {
    var c, _endTime;
    var d, action = {
        start: function(a) {
            c = new Date();
            if (window.LoadData && a != "Refresh Chart") {
                window.LoadData()
            }
        },
        end: function(m) {
            if (document.getElementById("load_dbg")) {
                document.getElementById("load_dbg").innerHTML = m + " 耗时:" + (new Date() - c) + "ms"
            }
        },
        timeout: function() {}
    };
    d = action;
    window.refresh = d;
    window.LoadData = function() {
        refreshChart()
    };
    function getUrlParam(a) {
        var b = new RegExp("(^|&)" + a + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(b);
        if (r != null) return unescape(r[2]);
        return null
    }
    function initTabs() {
        var a = '';
        a = a == null ? window["dCode"] : a;
        window["scode"] = a;
        d.start()
    }
    initTabs()
})();
function FillRect(a, b, c, d, e, f) {
    a.fillStyle = f;
    a.fillRect(b, c, d, e)
}
function DrawRect(a, b, c, d, e, f) {
    a.strokeStyle = f;
    a.beginPath();
    a.moveTo(b, c);
    a.lineTo(b + d, c);
    a.lineTo(b + d, c + e);
    a.lineTo(b, c + e);
    a.lineTo(b, c);
    a.stroke()
}
function FillLinearRect(a, b, c, d, e, f) {
    var g = a.createLinearGradient(b, c, b + d, c);
    for (var i = 0; i < f.length; i++) {
        g.addColorStop(f[i][0], f[i][1])
    }
    a.fillStyle = g;
    a.fillRect(b, c, d, e)
}
function DrawLine(a, b, c, d, e, f) {
    if (f == "") {
        a.strokeStyle = "#000000"
    } else {
        a.strokeStyle = f
    }
    a.beginPath();
    a.moveTo(b, c);
    a.lineTo(d, e);
    a.stroke()
}
function DrawLines(a, b, c) {
    if (c == "") {
        a.strokeStyle = "#000000"
    } else {
        a.strokeStyle = c
    }
    if (b.length > 0) {
        a.beginPath();
        var d = new Array();
        d = b[0];
        a.moveTo(d[0], d[1]);
        for (var i = 1; i < b.length; i++) {
            d = b[i];
            a.lineTo(d[0], d[1])
        }
        a.stroke()
    }
}
function DrawString(a, b, c, d, e, f, x, y) {
    if (c == "") {
        a.font = "12px 微软雅黑"
    } else {
        a.font = c
    }
    if (d == "") {
        a.fillStyle = "#000000"
    } else {
        a.fillStyle = d
    }
    if (e == "") {
        a.textAlign = "left"
    } else {
        a.textAlign = e
    }
    if (f == "") {
        a.textBaseline = "top"
    } else {
        a.textBaseline = f
    }
    a.fillText(b, x, y)
}
function Max(a) {
    return Math.max.apply(Math, a)
}
function Min(a) {
    return Math.min.apply(Math, a)
}
function MinGongBeiShu(a, b) {
    var c = Math.min(a, b),
        maxNum = Math.max(a, b),
        i = c,
        vper = 0;
    if (a === 0 || b === 0) {
        return maxNum
    }
    for (; i <= maxNum; i++) {
        vper = c * i;
        if (vper % maxNum === 0) {
            return vper;
            break
        }
    }
}
function GetYaxis(a) {
    var b = Max(a);
    var c = Min(a);
    return [b, c]
}
function DrawColumnChart(a, b, c, d, e, f, g, h) {
    var j = Number(a.getAttribute('height'));
    var k = Number(a.getAttribute('width'));
    var l = a.getContext("2d");
    l.fillStyle = "#f0f0f0";
    l.clearRect(0, 0, k, j);
    if (g == null || g.length == 0) {
        DrawString(l, "无数据", b, "#000000", "center", "middle", k / 2, j / 2)
    } else {
        var i = 0;
        var m = new Array();
        for (i = 0; i < g.length; i++) {
            m[i] = g[i][1]
        }
        var n = Max(m);
        var o = Min(m);
        var p = Math.abs(n);
        var q = Math.abs(o);
        var r = 0;
        var s = 0;
        var t;
        if (n > 0 && o < 0) {
            t = 0;
            r = n - o
        } else {
            if (n > 0) {
                t = 1
            } else {
                t = 2
            }
            if (p > q) {
                r = p
            } else {
                r = q
            }
            s = 20
        }
        var u = (j - c - e - 1 - s) / r;
        var v;
        if (t == 0) {
            v = parseInt(c + u * n)
        } else if (t == 1) {
            v = j - e - s
        } else if (t == 2) {
            v = c + s
        }
        DrawLine(l, f, v, k - d, v, "");
        var w = 1;
        var x = (k - (f + d)) / (3 * g.length + 1);
        var y = new Array();
        var z = 1;
        if (g.length > 10) {
            for (i = 10; i > 1; i--) {
                if (g.length % i < 2) {
                    z = parseInt(g.length / i);
                    break
                }
            }
        }
        var A = GetYaxis(m);
        for (i = 0; i < g.length; i++) {
            y[i] = new Array();
            var B = parseFloat(g[i][1]);
            y[i][0] = f + (i * 3 + 1) * x;
            var C = -B * u;
            y[i][1] = v;
            var D = "#000";
            var E = 0;
            if (B > 0) {
                E = 6;
                D = "#CF2E2E"
            } else if (B < 0) {
                E = -18;
                D = "#339900"
            } else {
                E = 6
            }
            FillRect(l, y[i][0], y[i][1], x * 2, C, D);
            var F = g[i][0];
            var G = g[i][1];
            DrawString(l, G, h, D, "center", "top", f + (i * 3 + 2) * x, v + E);
            DrawString(l, F, h, "", "center", "top", f + (i * 3 + 2) * x, j - e + 3)
        }
    }
}

function gt_iOS4() {
    if ((navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i))) {
        return Boolean(navigator.userAgent.match(/OS [1-4]_\d[_\d]* like Mac OS X/i))
    } else {
        return false
    }
}
function setClass(obj, classname) {
    if (obj && typeof obj == "string" && classname && typeof classname == "string") {
        document.getElementById(obj) ? document.getElementById(obj).setAttribute("class", classname) : "";
        document.getElementById(obj) ? document.getElementById(obj).setAttribute("className", classname) : ""
    }
    if (typeof obj == "object" && typeof classname == "object") {
        for (var i = 0; i < obj.length; i++) {
            obj[i] ? obj[i].setAttribute("class", classname[i]) : "";
            obj[i] ? obj[i].setAttribute("className", classname[i]) : ""
        }
    }
    if (typeof obj == "object" && classname && typeof classname == "string") {
        obj ? obj.setAttribute("class", classname) : "";
        obj ? obj.setAttribute("className", classname) : ""
    }
}
function setFlash(obj, cls1, cls2) {
    this.setTimeout(function() {
            setClass(obj, cls1)
        },
        800);
    this.setTimeout(function() {
            setClass(obj, cls2)
        },
        1000);
    this.setTimeout(function() {
            setClass(obj, cls1)
        },
        1200);
    this.setTimeout(function() {
            setClass(obj, cls2)
        },
        1400)
}
function getValueWithUnit(val) {
    var unit = "";
    if (isNaN(val)) {
        return "-"
    } else if (val >= 1e4) {
        val = parseFloat(val);
        if (val >= 1e8) {
            val = val / 1e8;
            var valInt = parseInt(val);
            if (valInt >= 1e3) {
                val = valInt
            } else if (valInt >= 100) {
                val = val.toFixed(1)
            } else {
                val = val.toFixed(2)
            }
            unit = "亿"
        } else {
            val = val / 1e4;
            var valInt = parseInt(val);
            if (valInt >= 1e3) {
                val = valInt
            } else if (valInt >= 100) {
                val = val.toFixed(1)
            } else {
                val = val.toFixed(2)
            }
            unit = "万"
        }
        return val + unit
    } else {
        return val
    }
}
function getIntValue(val) {
    var unit = "";
    if (isNaN(val)) {
        return "-"
    } else {
        val = parseFloat(val);
        if (val >= 1e8) {
            val = val / 1e8;
            var valInt = parseInt(val);
            if (valInt >= 1e3) {
                val = valInt
            } else if (valInt >= 100) {
                val = val.toFixed(1)
            } else {
                val = val.toFixed(2)
            }
            unit = "亿";
            return val + unit
        } else {
            return val.toFixed(0)
        }
    }
}
function getCjmxValue(val) {
    var unit = "";
    if (isNaN(val)) {
        return "-"
    } else {
        val = parseFloat(val);
        if (val >= 1e4) {
            val = val / 1e4;
            return val.toFixed(0) + "万"
        } else {
            return val
        }
    }
}
function getJiduByDate(date) {
    var jidu = "";
    var m = date.split('-')[1];
    switch (m) {
        case "03":
            jidu = "(一)";
            break;
        case "06":
            jidu = "(二)";
            break;
        case "09":
            jidu = "(三)";
            break;
        case "12":
            jidu = "(四)";
            break
    }
    return jidu
}
function getTradeDetails(closedPrice, details, count) {
    var html = [];
    if (details.length == 0 || (details.length == 1 && details[0] == "-")) {
        return "<tr><td>-</td><td>-</td><td>-</td></tr>"
    }
    for (var i = 0; i < details.length; i++) {
        if (i >= count) {
            break
        }
        var data = details[i].split(',');
        html.push("<tr>");
        if (count > 10) {
            html.push("<td>" + data[0] + "</td>")
        } else {
            html.push("<td>" + data[0].substring(0, data[0].length - 3) + "</td>")
        }
        var val = data[1] * 1;
        var valColor = "";
        if (val > closedPrice) {
            valColor = " class=\"font_red\""
        } else if (val < closedPrice) {
            valColor = " class=\"font_green\""
        }
        html.push("<td" + valColor + ">" + data[1] + "</td>");
        var direction = "";
        var cjColor = "";
        val = data[3] * 1;
        if (val == 1) {
            cjColor = " class=\"font_red\"";
            direction = "↑"
        } else if (val == -1) {
            cjColor = " class=\"font_green\"";
            direction = "↓"
        }
        if (count > 10) {
            html.push("<td" + cjColor + ">" + data[2] + direction + "</td>")
        } else {
            html.push("<td" + cjColor + ">" + getCjmxValue(data[2]) + direction + "</td>")
        }
        html.push("</tr>")
    }
    return html.join("")
}
function getOptionTradeDetails(closedPrice, details, count) {
    var dic = ["双开", "双平", "多换", "多开", "多平", "空换", "空开", "空平", "未知"];
    var dicClass = ["font_red", "font_red", "font_red", "font_red", "font_green", "font_green", "font_green", "font_green", ""];
    var html = [];
    if (details.length == 0 || (details.length == 1 && details[0] == "-")) {
        return "<tr><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>"
    }
    for (var i = 0; i < details.length; i++) {
        if (i >= count) {
            break
        }
        var data = details[i].split(',');
        html.push("<tr>");
        if (count > 10) {
            html.push("<td>" + data[0] + "</td>")
        } else {
            html.push("<td>" + data[0].substring(0, data[0].length - 3) + "</td>")
        }
        var val = data[1] * 1;
        var valColor = "";
        if (val > closedPrice) {
            valColor = " class=\"font_red\""
        } else if (val < closedPrice) {
            valColor = " class=\"font_green\""
        }
        html.push("<td" + valColor + ">" + data[1] + "</td>");
        var direction = "";
        var cjColor = "";
        val = data[3] * 1;
        if (val == 1) {
            cjColor = " class=\"font_red\"";
            direction = "↑"
        } else if (val == -1) {
            cjColor = " class=\"font_green\"";
            direction = "↓"
        }
        if (count > 10) {
            html.push("<td" + cjColor + ">" + data[2] + direction + "</td>")
        } else {
            html.push("<td" + cjColor + ">" + getCjmxValue(data[2]) + direction + "</td>")
        }
        html.push("<td>" + data[4] + "</td>");
        var dicIndex = data[5] == -1 ? 8 : data[5];
        html.push("<td class=\"" + dicClass[dicIndex] + "\">" + dic[dicIndex] + "</td>");
        html.push("</tr>")
    }
    return html.join("")
}
function getSingleFivePan(index, closedPrice, data) {
    var html = [];
    html.push("<tr>");
    html.push("<td>" + index + "</td>");
    var val = data[0] * 1;
    var valColor = "";
    if (val > closedPrice) {
        valColor = " class=\"font_red\""
    } else if (val < closedPrice) {
        valColor = " class=\"font_green\""
    }
    html.push("<td" + valColor + ">" + data[0] + "</td>");
    html.push("<td>" + data[1] + "</td>");
    html.push("</tr>");
    return html.join("")
}
function getFivePan(closedPrice, details) {
    var sellhtml = [];
    var sellIndex = 5;
    var buyhtml = [];
    var buyIndex = 1;
    for (var i = 0; i < 5; i++) {
        var data = details[i].split(',');
        buyhtml.push(getSingleFivePan(buyIndex++, closedPrice, data))
    }
    for (var i = details.length - 1; i >= details.length - 5; i--) {
        var data = details[i].split(',');
        sellhtml.push(getSingleFivePan(sellIndex--, closedPrice, data))
    }
    return [sellhtml.join(""), buyhtml.join("")]
}
function getZhengNumber(n) {
    n = n.toString();
    if (n.indexOf("-") == 0) {
        n = n.substring(1)
    }
    return n
}
function trigger(element, eventName) {
    if (typeof element == "undefined" || element == null) return;
    if (document.all) {
        element[eventName]()
    } else {
        var evt = document.createEvent("MouseEvents");
        evt.initEvent(eventName, true, true);
        element.dispatchEvent(evt)
    }
}
function getCookie(key) {
    var result = document.cookie.match(new RegExp("(^| )" + key + "=([^;]*)"));
    return result != null ? unescape(decodeURI(result[2])) : null
}
function getUid() {
    var webPi = getCookie("pi");
    if (webPi && webPi.split(';').length >= 3) {
        var uid = webPi.split(';')[0];
        if (uid.length == 16) {
            return uid
        }
    }
    return ""
}
function getScrollTop() {
    return Math.max(document.documentElement.scrollTop, document.body.scrollTop)
}
function getScrollHeight() {
    var scrollHeight = 0,
        bodyScrollHeight = 0,
        documentScrollHeight = 0;
    if (document.body) {
        bodyScrollHeight = document.body.scrollHeight
    }
    if (document.documentElement) {
        documentScrollHeight = document.documentElement.scrollHeight
    }
    scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight: documentScrollHeight;
    return scrollHeight
}
function getWindowHeight() {
    var windowHeight = 0;
    if (document.compatMode == "CSS1Compat") {
        windowHeight = document.documentElement.clientHeight
    } else {
        windowHeight = document.body.clientHeight
    }
    return windowHeight
}
function getWindowSize() {
    var myWidth = 0,
        myHeight = 0;
    if (typeof(window.innerWidth) == 'number') {
        myWidth = window.innerWidth;
        myHeight = window.innerHeight
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight
    }
    return ([myWidth, myHeight])
}
function intval(v) {
    v = parseInt(v);
    return isNaN(v) ? 0 : v
}
function getPos(e) {
    var l = 0;
    var t = 0;
    var w = intval(e.style.width);
    var h = intval(e.style.height);
    var wb = e.offsetWidth;
    var hb = e.offsetHeight;
    while (e.offsetParent) {
        l += e.offsetLeft + (e.currentStyle ? intval(e.currentStyle.borderLeftWidth) : 0);
        t += e.offsetTop + (e.currentStyle ? intval(e.currentStyle.borderTopWidth) : 0);
        e = e.offsetParent
    }
    l += e.offsetLeft + (e.currentStyle ? intval(e.currentStyle.borderLeftWidth) : 0);
    t += e.offsetTop + (e.currentStyle ? intval(e.currentStyle.borderTopWidth) : 0);
    return {
        x: l,
        y: t,
        w: w,
        h: h,
        wb: wb,
        hb: hb
    }
}